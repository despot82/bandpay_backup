import { StgBandpayClientPage } from './app.po';

describe('stg-bandpay-client App', () => {
  let page: StgBandpayClientPage;

  beforeEach(() => {
    page = new StgBandpayClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
