// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  // API related data.
  // TODO: Find a better way to secure credentials.
  api: {
    url: 'https://api.gigpay.ch/v1',
    //url: 'http://localhost/bandpay/stg-bandpay-api/v1',
    
    credentials: 'stagend:MySecureAccess666',
    secret: 'secret'
  },
  // PostFinance e-payment.
  postFinance: {
    url: 'https://e-payment.postfinance.ch/ncol/test/orderstandard_utf8.asp',
    // PostFinance SHA-IN shaIn.
    shaIn: 'MySafeTransaction666',
    shaOut: 'rWjBj32B?!7D)hUab@mad',
    paymentMethods: [
      { pm: 'PostFinance+Card', brand: 'PostFinance+Card' },
      { pm: 'PostFinance+e-finance', brand: 'PostFinance+e-finance' },
      { pm: 'CreditCard', brand: 'VISA' },
      { pm: 'CreditCard', brand: 'MasterCard' },
      { pm: 'PAYPAL', brand: 'PAYPAL' }
    ]
  }
};
