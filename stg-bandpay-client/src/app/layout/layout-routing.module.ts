import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { HomepageComponent } from '../bandpay/vendor-area/homepage/homepage.component';
import { VendorAreaComponent } from '../bandpay/vendor-area/vendor-area.component';
import { AdminAreaComponent } from '../bandpay/admin-area/admin-area.component';
import { TestComponent } from '../bandpay/test/test.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', component: HomepageComponent },
      { path: 'artist', component: VendorAreaComponent, loadChildren: '../bandpay/vendor-area/vendor-area.module#VendorAreaModule' },
      { path: 'admin', component: AdminAreaComponent, loadChildren: '../bandpay/admin-area/admin-area.module#AdminAreaModule' },
      { path: 'checkout', loadChildren: '../bandpay/buyer-area/buyer-area.module#BuyerAreaModule' },
      { path: 'form', loadChildren: '../forms/forms.module#MyFormsModule' },
      { path: 'pglayout', loadChildren: '../page-layouts/page-layouts.module#PageLayoutsModule' },
      { path: 'table', loadChildren: '../tables/tables.module#MyTablesModule' },
      { path: 'ui', loadChildren: '../ui/ui.module#UIModule' },
      { path: 'test', component: TestComponent }
    ]
  }
];

export const LayoutRoutingModule = RouterModule.forChild(routes);
