import { NgModule } from '@angular/core';

import { LayoutRoutingModule } from './layout-routing.module';
import { VendorAreaModule } from "../bandpay/vendor-area/vendor-area.module";
import { AdminAreaModule } from "../bandpay/admin-area/admin-area.module";

@NgModule({
  imports: [
    LayoutRoutingModule,

    VendorAreaModule,
    AdminAreaModule
  ],
  declarations: []
})

export class LayoutModule {}
