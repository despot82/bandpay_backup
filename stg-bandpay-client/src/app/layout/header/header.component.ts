import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../config';
import { TranslateService } from 'ng2-translate';
import { DatastoreService } from '../../bandpay/shared/datastore.service';
import { IntercommService } from '../../bandpay/shared/intercomm.service';

@Component({
  selector: 'my-app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class AppHeaderComponent implements OnInit {
    public AppConfig: any;
    private selectedLanguage: string;
    
    constructor(
        private datastore: DatastoreService,
        private translate: TranslateService,
        private intercomm: IntercommService) {
    
    }
    
    ngOnInit() {
      this.AppConfig = APPCONFIG;
      this.selectedLanguage = this.datastore.getValue('language');
      
      
    }
  
    onLanguageClicked(language) {
        this.datastore.setValue('language', language);
        this.selectedLanguage = language;
        
        this.intercomm.selectedLanguage.next(language);
    }
    
      
}
