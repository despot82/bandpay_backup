import { Directive, HostListener, Input, Output, AfterViewInit, AfterContentChecked, EventEmitter } from '@angular/core';
import { RestService } from '../shared/rest.service';
import { Headers, ResponseContentType } from '@angular/http';

@Directive({
  selector: 'button[pdfButton],input[type=button][pdfButton]'
})
export class PdfButtonDirective {
  @Input() pdfEl: any;

  @Output() emitPdfUrl = new EventEmitter();
  
  @Output() emitSubmissionRequest = new EventEmitter();
  
  url: string;

  constructor(private rest: RestService) { }
 
  @HostListener('click')
  onClick() {
    this.getPdf();
  }

  public getPdf() {
    
    let html = "<link rel='stylesheet' href='https://gigpay.ch/web/invoice-pdf-css/styles.css'>";
    html += "<link rel='stylesheet' href='https://gigpay.ch/web/invoice-pdf-css/bootstrap.css'>";
    html += this.pdfEl.innerHTML;
    
    //alert("innerHtml is: " + this.pdfEl.innerHTML);
    
    this.rest.post(
      '/pdf',
      {html},
      {
        headers: new Headers(),
        responseType: ResponseContentType.Json
      }
    ).subscribe(
      (result) => {
        
        let parsed = result.json(); 
        this.url = "https://gigpay.ch/web/viewer.html?file=pdfs/" + parsed[0];
        this.emitPdfUrl.emit(this.url);
        this.emitSubmissionRequest.emit();
        
    });
     
  }
}
