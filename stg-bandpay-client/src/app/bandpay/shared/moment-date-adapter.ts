import { DateAdapter, MdDateFormats } from '@angular/material';
import * as moment from 'moment';
import { Moment, LocaleSpecifier } from 'moment';

export class MomentDateAdapter extends DateAdapter<Moment> {
  getYear(date: Moment): number {
    return date.year();
  }
  getMonth(date: Moment): number {
    return date.month();
  }
  getDate(date: Moment): number {
    return date.date();
  }
  getDayOfWeek(date: Moment): number {
    return date.day();
  }
  getMonthNames(style: 'long' | 'short' | 'narrow'): string[] {
    switch (style) {
      case 'long':
        return moment.months();
      case 'short':
        return moment.monthsShort();
      case 'narrow':
        return moment.monthsShort()
          .map((month) => month.substring(0, 1));
    }
  }
  getDateNames(): string[] {
    return Array.from({ length: 31 }, (v, k) => String(k + 1));
  }
  getDayOfWeekNames(style: 'long' | 'short' | 'narrow'): string[] {
    switch (style) {
      case 'long':
        return moment.weekdays();
      case 'short':
        return moment.weekdaysShort();
      case 'narrow':
        return moment.weekdaysMin()
          .map((day) => day.substring(0, 1));
    }
  }
  getYearName(date: Moment): string {
    return String(date.year());
  }
  getFirstDayOfWeek(): number {
    return moment.localeData().firstDayOfWeek();
  }
  getNumDaysInMonth(date: Moment): number {
    return date.daysInMonth();
  }
  clone(date: Moment): Moment {
    return date.clone();
  }
  createDate(year: number, month: number, date: number): Moment {
    return moment({ year, month, date });
  }
  today(): Moment {
    return moment();
  }
  parse(value: any, parseFormat: any): Moment {
    return moment(value, parseFormat);
  }
  format(date: Moment, displayFormat: any): string {
    return date.format(displayFormat);
  }
  addCalendarYears(date: Moment, years: number): Moment {
    return date.add({ years });
  }
  addCalendarMonths(date: Moment, months: number): Moment {
    return date.add({ months });
  }
  addCalendarDays(date: Moment, days: number): Moment {
    return date.add({ days });
  }
  getISODateString(date: Moment): string {
    return date.toISOString();
  }
}

export const MOMENT_DATE_FORMATS: MdDateFormats = {
  parse: {
    dateInput: 'L'
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};
