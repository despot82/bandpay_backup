import { NgModule } from '@angular/core';
import { GooglePlacesAutocompleteDirective } from './google-places-autocomplete.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { Ng2Webstorage } from 'ngx-webstorage/dist/app';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { PhoneCodeSelectorDirective } from './phone-code-selector.directive';
import { SlotComponent } from './invoice/slot/slot.component';
import { BuyerComponent } from './invoice/buyer/buyer.component';
import { VendorComponent } from './invoice/vendor/vendor.component';
import { CommonModule } from '@angular/common';
import { TransactionTableComponent } from './invoice/transaction-table/transaction-table.component';
import { PdfButtonDirective } from './pdf-button.directive';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { MdPhoneCodeSelectorComponent } from './md-phone-code-selector.component';
import { InvoiceComponent } from './invoice/invoice.component';

import { TransactionDetailsComponent } from './invoice/transaction-details/transaction-details.component';
import { PrettyJsonModule } from 'angular2-prettyjson';

//Translations
import { TranslateModule } from 'ng2-translate';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    AngularFontAwesomeModule,

    // DEBUG
    PrettyJsonModule,
    
    //Translation
    TranslateModule
    
  ],
  declarations: [
    GooglePlacesAutocompleteDirective,
    PhoneCodeSelectorDirective,
    PdfButtonDirective,
    SlotComponent,
    BuyerComponent,
    VendorComponent,
    TransactionTableComponent,
    TimepickerComponent,
    MdPhoneCodeSelectorComponent,
    InvoiceComponent,
    
    TransactionDetailsComponent,
  ],
  exports: [
    GooglePlacesAutocompleteDirective,
    PhoneCodeSelectorDirective,
    PdfButtonDirective,
    SlotComponent,
    BuyerComponent,
    VendorComponent,
    TransactionTableComponent,
    TimepickerComponent,
    MdPhoneCodeSelectorComponent,
    InvoiceComponent,
    
    TransactionDetailsComponent,

    CommonModule,
    FormsModule,
    MaterialModule,
    AngularFontAwesomeModule,

    // DEBUG
    PrettyJsonModule,
    TranslateModule
  ]
})

export class SharedModule { }
