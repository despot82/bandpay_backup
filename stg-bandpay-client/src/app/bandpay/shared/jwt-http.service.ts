import { Injectable } from '@angular/core';
import { Http, Request, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LocalStorage } from 'ngx-webstorage';
import * as jwtDecode from 'jwt-decode';
import * as moment from 'moment';

@Injectable()
export class JwtHttp {
  @LocalStorage()
  private token: string;

  private http: Http;

  constructor(http: Http) {
    this.http = http;
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.request(url, options);
  }
  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.get(url, options);
  }
  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.post(url, body, options);
  }
  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.put(url, body, options);
  }
  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.delete(url, options);
  }
  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.patch(url, body, options);
  }
  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.head(url, options);
  }
  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    options = this.attachToken(options);
    return this.http.options(url, options);
  }

  decodeToken(): any {
    return jwtDecode(this.token);
  }

  tokenExists(): boolean {
    return this.token != null && this.token.length > 0;
  }

  isTokenExpired(): boolean {
    const now = moment();
    const exp = moment.unix(this.decodeToken().exp);
    return now.isAfter(exp);
  }

  saveToken(newToken: string) {
    this.token = newToken;
  }

  private attachToken(options?: RequestOptionsArgs) {
    if (options) {
      options.headers.append('Authorization', 'Bearer ' + this.token);
    } else {
      const headers = new Headers({
        Authorization: 'Bearer ' + this.token
      });
      options = {
        headers
      };
    }
    return options;
  }
}
