import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { NgbTime } from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';
import { } from 'googlemaps';
import * as moment from 'moment';
import { Moment } from 'moment';

export class Slot {
  readonly maxDuration = 32400;
  readonly policies = [
    { value: 'standard', caption: 'Standard', description: 'Full refund up until 30 hours prior to event. 50% refund from the 30th hour prior the event up to the event moment. Except fees.<br>Example: if the organizer cancels the event 30 hours prior to the event, he gets back 100% of the cachet. If he cancels later, the artist takes 50% of the cachet.' },
    { value: 'moderate', caption: 'Moderate', description: 'Full refund up to 4 weeks before the event. Refund of 50% from 4 to 2 weeks prior to the event. Service costs excluded.<br>Example: if the organizer cancels the event four weeks prior will be fully refunded. If the event is canceled two weeks before the artist will receive 50% of the amount, and if the event is canceled during the 2 weeks before the event the artist will receive the entire the amount.' },
    // {value: 'strict', caption: 'Strict', description: 'No refund provided before the event in case of cancellation by the organizer. Exceptions to this policy may be granted only on exceptional and justified events.<br>Example: if the organizer cancels the event the full amount established will be paid to the artist.'}
  ];
  readonly categories = [
    'Background Music',
    'Birthday',
    'Ceremony',
    'Club Event',
    'Corporate Event',
    'Event',
    'Festival',
    'Party',
    'Party for Kids',
    'Religious Ceremony',
    'Seasonal Event',
    'Wedding'
  ];

  startTime: Moment;
  category: string;
  // TODO: Implement duration using Moment.Duration interface.
  duration: number;
  street: string;
  zip: string;
  city: string;
  country: string;
  guestsNumber: number;
  audioSystem: boolean;
  lightingSystem: boolean;
  cateringIncluded: number;
  accomodationIncluded: number;
  cancellationPolicy: string;

  transaction: Transaction;

  constructor() {
    
    this.startTime = moment();
    this.audioSystem = false;
    this.lightingSystem = false;
    this.cancellationPolicy = this.policies[0].value;

    this.transaction = new Transaction();
  }
 
  setStartTime(date: Date) {
    this.startTime = moment(date);
  }
 
  // The two duration inputs are bound by the duration member and depend on each other.
  get durationHours() {
    if (!this.duration) {
      return null;
    }
    return Math.floor(this.duration / 3600);
  }

  set durationHours(hours: number) {
    this.duration = Math.min(hours * 3600 + this.durationMinutes * 60, this.maxDuration);
  }

  get durationMinutes() {
    if (!this.duration) {
      return null;
    }
    return (this.duration - this.durationHours * 3600) / 60;
  }

  set durationMinutes(minutes: number) {
    this.duration = Math.min(this.durationHours * 3600 + minutes * 60, this.maxDuration);
  }

  get policyCaption(): string {
    return this.policies.find((cur) => cur.value === this.cancellationPolicy).caption;
  }

  get policyDescription(): string {
    return this.policies.find((pol) => pol.value === this.cancellationPolicy).description;
  }
}

// tslint:disable-next-line:max-classes-per-file
export class Transaction {
  readonly currencies = [
    { value: 'chf', caption: 'CHF' },
    { value: 'eur', caption: 'EUR' }
  ];
  readonly paymentMethods: Array<{ pm: string, caption: string }> = [
    { pm: 'VISA', caption: 'VISA <i style="vertical-align: middle;" class="fa fa-cc-visa fa-3x ml-1"></i>' },
    { pm: 'MasterCard', caption: 'MasterCard <i style="vertical-align: middle;" class="fa fa-cc-mastercard fa-3x ml-1"></i>' },
    { pm: 'PAYPAL', caption: 'PayPal <i style="vertical-align: middle;" class="fa fa-paypal fa-3x ml-1"></i>' },
    { pm: 'PostFinance+Card', caption: 'PostFinance Card <img style="height: 3em;" class="ml-1" src="../../../assets/bandpay/images/postfinance-card.png">' },
    { pm: 'PostFinance+e-finance', caption: 'PostFinance E-Finance <img style="height: 3em;" class="ml-1" src="../../../assets/bandpay/images/efinance.png">' },
    { pm: 'invoice', caption: 'Invoice <i style="vertical-align: middle;" class="fa fa-file-text fa-3x ml-1"></i>' },
  ];
  readonly statuses = [
    { value: 'to_be_payed', caption: 'To be payed' },
    { value: 'payed', caption: 'Payed' }
  ];

  amount: number;
  cachet: number;
  fee: number;
  currency: string;
  paymentMethod: string;
  status: string;
  created: Moment;
  paymentDue: Moment;

  constructor() {
    this.currency = this.currencies[0].value;
    this.paymentMethod = this.paymentMethods[0].pm;
    this.status = this.statuses[0].value;
    
    // DEV:  TO DO: to display or not display: if(this.amount < 4000) this.paymentMethods.push({ pm: 'MFG', caption: 'MFG <i style="vertical-align: middle;" class="fa fa-cc-visa fa-3x ml-1"></i>' });
    
  }

  get formattedAmount() {
    if (this.amount) {
      return `${this.amount.toFixed(2)} ${this.currencyCaption}`;
    }
  }

  get formattedCachet() {
    if (this.cachet) {
      return `${this.cachet.toFixed(2)} ${this.currencyCaption}`;
    }
  }

  get formattedFee() {
    if (this.fee) {
      return `${this.fee.toFixed(2)} ${this.currencyCaption}`;
    }
  }

  private get currencyCaption() {
    return this.currencies.find((cur) => cur.value === this.currency).caption;
  }
}
