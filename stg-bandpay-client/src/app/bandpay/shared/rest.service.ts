import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptionsArgs } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/bp-environment';
import { JwtHttp } from './jwt-http.service';

@Injectable()
export class RestService {
  private apiUrl: string;

  constructor(private http: Http, private jwtHttp: JwtHttp) {
    this.apiUrl = environment.api.url;
    if (!this.jwtHttp.tokenExists() || this.jwtHttp.isTokenExpired()) {
      this.refreshToken();
    }
    console.log('Rest service initialized.');
  }

  refreshToken(): void {
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa(environment.api.credentials)
    });
    const body = { secret: environment.api.secret };
    this.http.post(
      this.apiUrl + '/token',
      body,
      {
        headers
      }
    )
      .catch(this.handleError.bind(this))
      .subscribe((value) => this.jwtHttp.saveToken((value as Response).json().token));
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.jwtHttp.get(this.apiUrl + url, options);
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.jwtHttp.post(this.apiUrl + url, body, options);
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.jwtHttp.put(this.apiUrl + url, body, options);
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.jwtHttp.delete(this.apiUrl + url, options);
  }

  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.jwtHttp.patch(this.apiUrl + url, body, options);
  }

  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.jwtHttp.head(this.apiUrl + url, options);
  }

  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.jwtHttp.options(this.apiUrl + url, options);
  }

  // TODO: Rewrite this.
  private handleError(error: Response | any) {
    return Observable.throw(error);
  }
}
