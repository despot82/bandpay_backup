import { Component, OnInit, forwardRef, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'md-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss'],

  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => TimepickerComponent),
      multi: true
    }
  ]
})
export class TimepickerComponent implements ControlValueAccessor {
  @ViewChild('hoursInput')
  hoursInput: ElementRef;
  @ViewChild('minutesInput')
  minutesInput: ElementRef;

  // tslint:disable-next-line:no-empty
  onChange = (_: any) => { };
  // tslint:disable-next-line:no-empty
  onTouched = () => { };

  private time: Moment;

  get minutes(): number {
    return this.time.minutes();
  }
  set minutes(minutes: number) {
    this.time.minutes(minutes);
    this.onChange(this.time);
  }

  get hours(): number {
    return this.time.hours();
  }
  set hours(hours: number) {
    this.time.hours(hours);
    this.onChange(this.time);
  }

  constructor(private renderer: Renderer2, private element: ElementRef) {
    this.time = moment();
  }

  writeValue(value: any): void {
    this.time = moment(value);
  }
  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.renderer.setProperty(this.hoursInput.nativeElement, 'disabled', isDisabled);
    this.renderer.setProperty(this.minutesInput.nativeElement, 'disabled', isDisabled);
  }
}
