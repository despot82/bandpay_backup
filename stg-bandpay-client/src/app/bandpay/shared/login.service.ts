import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { RestService } from './rest.service';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { JwtHttp } from './jwt-http.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {
  loggedIn: boolean;
  anonymous: boolean;
  userId: number;

  onLogin: ReplaySubject<void>;
  onLogout: ReplaySubject<void>;

  constructor(private rest: RestService, private jwtHttp: JwtHttp) {
    this.userId = this.extractUserId();
    this.loggedIn = this.userId ? true : false;
    this.anonymous = this.isUserAnonymous();
    this.onLogin = new ReplaySubject<void>();
    this.onLogout = new ReplaySubject<void>();
    if (this.loggedIn) {
      this.onLogin.next(null);
    }
    console.log('Login service initialized.');
  }

  login(credentials: { email: string, password: string }): Observable<Response> {
    return this.rest.post(
      '/stagend/vendor',
      credentials
    )
      .map(
      (value) => {
        // Get and save the token with login information.
        this.jwtHttp.saveToken(value.json().token);
        this.userId = this.extractUserId();

        this.loggedIn = true;
        this.anonymous = false;
        this.onLogin.next(null);
        return value;
      }
      );
  }

  anonymousLogin(credentials: { email: string }): Observable<Response> {
    return this.rest.post(
      '/vendor',
      credentials
    )
      .map(
      (value) => {
        this.jwtHttp.saveToken(value.json().token);
        this.userId = this.extractUserId();

        this.loggedIn = true;
        this.anonymous = true;
        this.onLogin.next(null);
        return value;
      });
  }

  logout() {
    this.loggedIn = false;
    this.anonymous = null;
    this.userId = null;
    this.onLogout.next(null);
    // Get and save an empty token.
    this.rest.refreshToken();
  }

  private extractUserId(): number {
    if (this.jwtHttp.tokenExists()) {
      return this.jwtHttp.decodeToken().user_id;
    } else {
      return null;
    }
  }

  private isUserAnonymous(): boolean {
    if (this.jwtHttp.tokenExists()) {
      return this.jwtHttp.decodeToken().anonymous;
    } else {
      return null;
    }
  }
}
