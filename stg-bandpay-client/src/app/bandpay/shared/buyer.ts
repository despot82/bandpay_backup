export class Buyer {
  firstName: string;
  lastName: string;
  company: string;
  email: string;
  phoneCode: string;
  phone: string;

  holder: string;
  
  street: string;
  zip: string;
  city: string;
  country: string;
  
  address: string;
}
