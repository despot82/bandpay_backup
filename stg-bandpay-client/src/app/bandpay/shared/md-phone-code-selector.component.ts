import { Component, OnInit, ElementRef, Renderer2, forwardRef, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Http } from '@angular/http';

@Component({
  selector: 'md-phone-code',
  template: `

  <div class="form-group">
    <md-select placeholder="Phone code" required [(ngModel)]="code" name="phoneCode">
      <md-option *ngFor="let pc of codes" [value]="pc.code">{{pc.country}} (+{{pc.code}})</md-option>
    </md-select>
  </div>`,

  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      // tslint:disable-next-line:no-forward-ref
      useExisting: forwardRef(() => MdPhoneCodeSelectorComponent),
      multi: true
    }
  ]
})
export class MdPhoneCodeSelectorComponent implements OnInit, ControlValueAccessor {
  readonly priority: string[] = ['CH', 'DE', 'IT', 'FR', 'US', 'GB', 'AU'];

  @ViewChild('input')
  input: ElementRef;

  codes: Array<{ country: string, code: string }>;

  _code: string;

  // tslint:disable-next-line:no-empty
  onChange = (_: any) => { };
  // tslint:disable-next-line:no-empty
  onTouched = () => { };

  get placeholder(): string {
    const placeholder = (this.element.nativeElement.attributes as NamedNodeMap).getNamedItem('placeholder');
    if (placeholder) {
      return placeholder.value;
    }
  }

  get code(): string {
    return this._code;
  }

  set code(code: string) {
    this._code = code;
    this.onChange(this.code);
  }

  constructor(private element: ElementRef, private renderer: Renderer2, private http: Http) { }

  ngOnInit() {
    this.populateCodes();
  }

  writeValue(value: any): void {
    this.code = value;
  }
  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.renderer.setProperty(this.input.nativeElement, 'disabled', isDisabled);
  }

  populateCodes() {
    this.http.get('https://restcountries.eu/rest/v2?fields=name%3Balpha2Code%3BcallingCodes')
      .subscribe((value) => {
        const body = value.json() as any[];
        let codes: Array<{ country: string, code: string }> = body
          // tslint:disable-next-line:no-shadowed-variable
          .map((value) => {
            return {
              country: value.alpha2Code,
              code: value.callingCodes[0]
            };
          })
          // tslint:disable-next-line:no-shadowed-variable
          .filter((value) => value.code != null);

        // Filter codes to be placed on top and sort them by the order in which they appear in priority array.
        const priorityCodes = codes
          // tslint:disable-next-line:no-shadowed-variable
          .filter((value) => this.priority.includes(value.country))
          .sort((a, b) => {
            const ia = this.priority.indexOf(a.country);
            const ib = this.priority.indexOf(b.country);
            return ia - ib;
          });

        codes = codes
          // tslint:disable-next-line:no-shadowed-variable
          .filter((value) => !priorityCodes.includes(value))
          .sort((a, b) => {
            // Just some filtering to have more pertinent ordering.
            const na = parseInt(a.code.replace(/ /g, ''), 10);
            const nb = parseInt(b.code.replace(/ /g, ''), 10);
            return na - nb;
          });
        this.codes = priorityCodes.concat(codes);
      });
  }
}
