import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { RestService } from '../rest.service';
import { DatastoreService } from '../datastore.service';
import { Buyer } from '../buyer';
import { VendorProfile, PaypalData, BankData } from '../vendor_profile';
import { Slot } from '../slot';
import { PdfButtonDirective } from '../pdf-button.directive';
import { Headers, ResponseContentType } from '@angular/http';

@Component({
  selector: 'invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent {
  
  vendorId: number;
  buyerId: number;
  private _slotId: number;
  
  @Input() set slotId(id: number) {
    this._slotId = id;
    this.populate();
  }
  
  private _isCheckoutPage: boolean;
  
  @Input() set isCheckoutPage(is: boolean) {
    this._isCheckoutPage = is;
  }

  @Output() emitPdfUrl = new EventEmitter();
  
  @Output() emitSubmissionRequest = new EventEmitter();

  constructor(private rest: RestService) { 
      this._isCheckoutPage = false;
  }
  
  get slotId(): number {
    return this._slotId;
  }


  private populate() {
    this.rest.get(`/slot/${this.slotId}?fields=id,vendor_id,buyer_id`)
      .subscribe(
      (value) => {
        const body = value.json();
        this.vendorId = body.vendor_id;
        this.buyerId = body.buyer_id;
      });
  }
  
  
  public getInvoicePdfUrl($event) {
    this.emitPdfUrl.emit($event);
  }
  
  public onSubmissionRequest($event) {
    this.emitSubmissionRequest.emit($event);
  }
}
