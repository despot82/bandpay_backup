import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { VendorProfile, PaypalData, BankData } from '../../vendor_profile';
import { RestService } from '../../rest.service';
import { DatastoreService } from '../../datastore.service';
import { Router } from '@angular/router';

@Component({
  selector: 'vendor',
  templateUrl: 'vendor.component.html',
  styleUrls: ['vendor.component.css'] 
})
export class VendorComponent {
  profile: VendorProfile;

  private _profileId: number;

  get profileId(): number {
    return this._profileId;
  }
  
  @Input('id') set profileId(id: number) {
    this._profileId = id;
    this.populate();
    this.datastore.setValue('profileId', id);
  }
  
  _hasEditLink: boolean; // If there should be an edit link on this sub-component
  
  @Input() set hasEditLink(is: boolean) {
    this._hasEditLink = is;
  }

  constructor(private rest: RestService, 
              private datastore: DatastoreService,
              private router: Router) {
    this.profileId = this.datastore.getValue('profileId');
    this.profile = new VendorProfile();
  }

  populate(): void {
    if (!this.profileId) {
      return;
    }
    this.rest.get(`/profile/${this.profileId}?embed=banking`)
      .subscribe(
      (value) => {
        const body = value.json();
        this.profile.name = body.name;
        this.profile.email = body.email;
        this.profile.phoneCode = body.phone_code;
        this.profile.phone = body.phone;
        if (body.paypal_data) {
          this.profile.bankingType = 'paypal';
          (this.profile.banking as PaypalData).email = body.paypal_data.email;
        } else if (body.bank_data) {
          this.profile.bankingType = 'bank';
          const bankData = this.profile.banking as BankData;
          bankData.beneficiary = body.bank_data.beneficiary;
          bankData.street = body.bank_data.street;
          bankData.zip = body.bank_data.zip;
          bankData.city = body.bank_data.city;
          bankData.iban = body.bank_data.iban;
        }
      });
  }
  
  public goToVendorStep() {
    this.router.navigate(['/artist/profile']);
  }
}
