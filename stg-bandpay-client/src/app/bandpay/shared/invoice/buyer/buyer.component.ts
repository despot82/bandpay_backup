import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { RestService } from '../../rest.service';
import { DatastoreService } from '../../datastore.service';
import { Buyer } from '../../buyer';
import { Router } from '@angular/router';

import { TranslateService, TranslatePipe } from 'ng2-translate';
import { IntercommService } from '../../../shared/intercomm.service';

@Component({
  selector: 'buyer',
  templateUrl: 'buyer.component.html',
  styleUrls: ['buyer.component.css'],
})
export class BuyerComponent {
  @HostBinding('class.col-6')
  readonly true;
  
  editable: boolean; // If the form should be editable at all, ie. should there be EDIT button
  
  @Input() set isInlineEditable(is: boolean) {
    this.editable = is;
  }
  
  private editing: boolean = false; // Is form data being edited or not

  buyer: Buyer;

  private _buyerId: number;

  get buyerId(): number {
    return this._buyerId;
  }

  @Input('id')
  set buyerId(id: number) {
    this._buyerId = id;
    this.populate();
    this.datastore.setValue('buyerId', id); 
  }

  constructor(
    private rest: RestService, 
    private datastore: DatastoreService,
    private router: Router,
    private translate: TranslateService,
    private intercomm: IntercommService
  ) {
    this.buyer = new Buyer();
    
    translate.use(this.datastore.getValue("language"));
    
    // Subscribe to the selectedLanguage Subject of the intercomm service
    this.intercomm.selectedLanguage.asObservable().subscribe((value: any) => {
        translate.use(value);
        
    });
  }
  
  private populate(): void {
    if (!this.buyerId) {
      return;
    }
    this.rest.get(`/buyer/${this.buyerId}`)
      .subscribe(
      (value) => {
        const body = value.json();
        this.buyer.firstName = body.first_name;
        this.buyer.lastName = body.last_name;
        this.buyer.email = body.email;
        this.buyer.phoneCode = body.phone_code;
        this.buyer.phone = body.phone;

        this.buyer.holder = body.holder;
        this.buyer.street = body.street;
        this.buyer.zip = body.zip;
        this.buyer.city = body.city;
        this.buyer.country = body.country;
      });
  }
  
  private goToOrganizerStep() {
    this.router.navigate(['/artist/organizer']);
  }
  
  private goEdit() {
    this.editing = true;
  }
  
  private saveBuyerData() {
    if(this.editing) {
      this.editing = false;
      
      let body = {
        first_name: this.buyer.firstName,
        last_name: this.buyer.lastName,
        street: this.buyer.street,
        zip: this.buyer.zip,
        city: this.buyer.city,
        country: this.buyer.country,
        
      };
      
      this.rest.patch(
          `/buyer/${this.buyerId}`,
          body
      ).subscribe(
          
      );
      
    }
  }
  
}
