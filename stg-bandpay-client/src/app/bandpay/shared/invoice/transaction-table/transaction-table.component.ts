import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from '../../slot';
import { RestService } from '../../rest.service';
import { DatastoreService } from '../../datastore.service';
import * as moment from 'moment';
import 'moment/locale/it';

@Component({
  selector: 'transaction-table',
  templateUrl: 'transaction-table.component.html',
  styleUrls: ['transaction-table.component.css']
})
export class TransactionTableComponent  {
  
  transaction: Transaction;

  feePercent: number;

  private _slotId: number;

  get slotId(): number {
    return this._slotId;
  }

  @Input()
  set slotId(id: number) {
    this._slotId = id;
    this.populate();
  }

  constructor(private datastore: DatastoreService, private rest: RestService) {
    this.slotId = this.datastore.getValue('slotId');
    this.transaction = new Transaction();
  }

  populate(): void {
    this.rest.get('/transaction/percentages')
      .subscribe(
      (value) => {
        this.feePercent = value.json().fee;
      });
    if (!this.slotId) {
      return;
    }
    this.rest.get(`/slot/${this.slotId}/transaction`)
      .subscribe(
      (value) => {
        const body = value.json();
        this.transaction.amount = body.amount;
        this.transaction.cachet = body.cachet;
        this.transaction.fee = body.fee;
        this.transaction.currency = body.currency;
        this.transaction.paymentDue = moment(body.payment_due);
      });
  }
  
}
