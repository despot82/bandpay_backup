import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Transaction } from '../../slot';
import { DatastoreService } from '../../datastore.service';
import { RestService } from '../../rest.service';
import * as moment from 'moment';

@Component({
  selector: 'transaction-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.css']
})
export class TransactionDetailsComponent {
  @HostBinding('class.col-6')
  readonly true;

  transaction: Transaction;

  private _slotId: number;

  get slotId(): number {
    return this._slotId;
  }

  @Input()
  set slotId(id: number) {
    this._slotId = id;
    this.populate();
  }

  constructor(private datastore: DatastoreService, private rest: RestService) {
    this.slotId = this.datastore.getValue('slotId');
    this.transaction = new Transaction();
  }

  private populate(): void {
    if (!this.slotId) {
      return;
    }
    this.rest.get(`/slot/${this.slotId}/transaction`)
      .subscribe(
      (value) => {
        const body = value.json();
        this.transaction.amount = body.amount;
        this.transaction.cachet = body.cachet;
        this.transaction.fee = body.fee;
        this.transaction.currency = body.currency;
        this.transaction.created = moment(body.created);
        this.transaction.paymentDue = moment(body.payment_due);
      });
  }
}
