import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Slot } from '../../slot';
import { parseDatetime } from '../../parsing-helper';
import { RestService } from '../../rest.service';
import { DatastoreService } from '../../datastore.service';
import * as moment from 'moment';
import { NgbTime } from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';
import { Router } from '@angular/router';

@Component({
  selector: 'slot',
  templateUrl: 'slot.component.html',
  styleUrls: ['slot.component.css'],
})
export class SlotComponent {
  slot: Slot;

  private _slotId: number;

  get slotId(): number {
    return this._slotId;
  }

  @Input('id')
  set slotId(id: number) {
    this._slotId = id;
    this.populate();
  }
  
  _hasEditLink: boolean; // If there should be an edit link on this sub-component
  
  @Input() set hasEditLink(is: boolean) {
    this._hasEditLink = is;
  }

  constructor(private rest: RestService, 
              private datastore: DatastoreService,
              private router: Router) {
    this.slotId = this.datastore.getValue('slotId');
    this.slot = new Slot();
  }

  get formattedStartTime(): string {
    const time = this.slot.startTime;
    const momentDate = moment().year(time.year()).month(time.month() - 1).date(time.date()).hour(time.hours()).minute(time.minutes());

    return momentDate.format('LLLL');
  }

  get formattedDuration(): string {
    if (this.slot.durationHours === 0) {
      return this.slot.durationMinutes + ' minutes';
    } else if (this.slot.durationMinutes === 0) {
      return this.slot.durationHours + ' hours';
    } else {
      return this.slot.durationHours + ' hours and ' + this.slot.durationMinutes + ' minutes';
    }
  }

  get cateringCaption(): string {
    return this.slot.cateringIncluded > 0 ? this.slot.cateringIncluded === 1 ? 'For 1 person' : `For ${this.slot.cateringIncluded} people` : 'Not needed';
  }

  get accomodationCaption(): string {
    return this.slot.accomodationIncluded > 0 ? this.slot.accomodationIncluded === 1 ? 'For 1 person' : `For ${this.slot.accomodationIncluded} people` : 'Not needed';
  }

  private populate(): void {
    if (!this.slotId) {
      return;
    }
    this.rest.get(`/slot/${this.slotId}?embed=transaction`)
      .subscribe(
      (value) => {
        const body = value.json();
        const datetime = moment(body.start_time);
        this.slot.startTime = datetime;
        this.slot.category = body.category;
        this.slot.duration = body.duration;
        this.slot.street = body.street;
        this.slot.zip = body.zip;
        this.slot.city = body.city;
        this.slot.country = body.country;
        this.slot.guestsNumber = body.guests_number;
        this.slot.audioSystem = body.audio_system == 1 ? true : false;
        this.slot.lightingSystem = body.lighting_system == 1 ? true : false;
        this.slot.cateringIncluded = body.catering_included;
        this.slot.accomodationIncluded = body.accomodation_included;
        this.slot.cancellationPolicy = body.cancellation_policy;

        this.slot.transaction.amount = body.transaction.amount;
        this.slot.transaction.currency = body.transaction.currency;
      });
  }
  
  public goToSlotStep() {
    this.router.navigate(['/artist/event']);
  }
}
