interface String {
	padStart(maxLength: number, fillString?: string): string;
	padEnd(maxLength: number, fillString?: string): string;
}

String.prototype.padStart = function (this: string, maxLength: number, fillString?: string): string {
	if (!fillString) {
		fillString = ' ';
	}
	return (fillString.repeat(maxLength).concat(this).slice(-maxLength));
};

String.prototype.padEnd = function (this: string, maxLength: number, fillString?: string) {
	if (!fillString) {
		fillString = ' ';
	}
	return (this.concat(fillString.repeat(maxLength)).slice(maxLength));
};