import { Directive, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { MapsAPILoader } from '@agm/core';

@Directive({
  selector: 'input[type=text][gpAutocomplete]'
})
export class GooglePlacesAutocompleteDirective {
  @Input()
  options: google.maps.places.AutocompleteOptions;

  @Output()
  placeChanged: EventEmitter<google.maps.places.PlaceResult>;

  constructor(private element: ElementRef, private mapsLoader: MapsAPILoader) {
    this.options = {types: ["(cities)"]};
    this.placeChanged = new EventEmitter();
    this.loadApi();
    
  }

  private loadApi() {
    this.mapsLoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.element.nativeElement, this.options);
      autocomplete.addListener('place_changed', () => {
        console.log(autocomplete.getPlace());
        this.placeChanged.emit(autocomplete.getPlace());
      });
      console.log('Google Places Autocomplete loaded.');
    });
  }

}
