import { Injectable } from '@angular/core';
import * as Collections from 'typescript-collections';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class IntercommService {
  
  selectedLanguage: Subject<string>;
  
  constructor() {
      this.selectedLanguage = new Subject<any>(); 
  }
 

}
