export class VendorProfile {
  readonly bankingTypes = [
    { value: 'paypal', caption: 'PayPal' },
    { value: 'bank', caption: 'Bank account' }
  ];

  name: string;
  email: string;
  phoneCode: string;
  phone: string;
  bankingType: string;
  banking: PaypalData | BankData;

  constructor() {
    this.bankingType = 'paypal';
    this.banking = {};
  }

  get address(): string {
    const bankData = this.banking as BankData;
    if (!bankData.city) {
      return null;
    }
    let address = bankData.city;
    if (bankData.street) {
      address = bankData.street + ', ' + address;
    }
    if (bankData.country) {
      address = address + ', ' + bankData.country;
    }
    return address;
  }
}

export interface PaypalData {
  email?: string;
}

export interface BankData {
  beneficiary?: string;
  street?: string;
  zip?: string;
  city?: string;
  country?: string;
  iban?: string;
}
