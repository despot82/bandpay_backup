export class StgUser {
  firstName: string;
  lastName: string;
  email: string;
  password: string;

  imageUrl: string;

  constructor() {
    this.firstName = null;
    this.lastName = null;
    this.email = null;
    this.password = null;
    this.imageUrl = null;
  }
}
