import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { Http } from '@angular/http';

@Directive({
  selector: 'input[phoneCodeSelector]'
})
export class PhoneCodeSelectorDirective {
  readonly priority: string[] = ['CH', 'DE', 'IT', 'FR', 'US', 'GB', 'AU'];

  codes: Array<{ country: string, code: string }>;

  constructor(private element: ElementRef, private renderer: Renderer2, private http: Http) {
    this.init();
  }

  private init() {
    this.http.get('https://restcountries.eu/rest/v2?fields=name%3Balpha2Code%3BcallingCodes')
      .subscribe((value) => {
        const body = value.json() as any[];
        let codes: Array<{ country: string, code: string }> = body.map((value) => {
          return {
            country: value.alpha2Code,
            code: value.callingCodes[0]
          };
        }).filter((value) => value.code != null);

        // Filter codes to be placed on top and sort them by the order in which they appear in priority array.
        const priorityCodes = codes.filter((value) => this.priority.includes(value.country)).sort((a, b) => {
          const ia = this.priority.indexOf(a.country);
          const ib = this.priority.indexOf(b.country);
          return ia - ib;
        });

        codes = codes.filter((value) => !priorityCodes.includes(value)).sort((a, b) => {
          // Just some filtering to have more pertinent ordering.
          const na = parseInt(a.code.replace(/ /g, ''));
          const nb = parseInt(b.code.replace(/ /g, ''));
          return na - nb;
        });
        this.codes = priorityCodes.concat(codes);

        const isMdEl = (this.element.nativeElement.attributes as NamedNodeMap).getNamedItem('mdInput') != null;
        if (isMdEl) {
          this.renderer.setAttribute(this.element.nativeElement, '[mdAutocomplete]', 'autocomplete');
          console.log(this.element.nativeElement);
        } else {
          this.renderer.setAttribute(this.element.nativeElement, 'list', 'phone-codes');
          this.renderer.appendChild(this.element.nativeElement, this.createDatalist());
        }
      });
  }

  private createDatalist(): any {
    const r = this.renderer;
    const datalist = r.createElement('datalist');
    r.setAttribute(datalist, 'id', 'phone-codes');
    r.setAttribute(datalist, 'name', 'phone-codes');

    // Populate the options for the phone codes.
    for (const code of this.codes) {
      const option = r.createElement('option');
      r.setAttribute(option, 'value', code.code);
      const caption = r.createText(`${code.country} (+${code.code})`);
      r.appendChild(option, caption);
      r.appendChild(datalist, option);
    }
    return datalist;
  }

  private createMdAutocomplete() {
    const r = this.renderer;

  }
}
