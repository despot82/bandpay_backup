import { Injectable } from '@angular/core';
import * as Collections from 'typescript-collections';

@Injectable()
export class DatastoreService extends Collections.Dictionary<string, any> {
  constructor() {
    super();
  }
}
