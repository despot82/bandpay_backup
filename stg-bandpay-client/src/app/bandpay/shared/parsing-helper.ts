import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { NgbTime } from '@ng-bootstrap/ng-bootstrap/timepicker/ngb-time';

export function parsePlace(place: google.maps.places.PlaceResult): {
  street: string,
  zip: string,
  city: string,
  region: string,
  country: string
} {
  const addressComponents = place.address_components;

  let street;
  let streetNumber;
  let streetName;
  let zip;
  let city;
  let region;
  let country;

  addressComponents.map((component) => {
    const types = component.types;
    if (types.includes('route')) {
      streetName = component.long_name;
    } else if (types.includes('street_number')) {
      streetNumber = component.long_name;
    } else if (types.includes('postal_code')) {
      zip = component.long_name;
    } else if (types.includes('locality')) {
      city = component.long_name;
    } else if (types.includes('administrative_area_level_1')) {
      region = component.long_name;
    } else if (types.includes('country')) {
      country = component.short_name;
    }
  });

  // If the street is defined use it, otherwise use simply the place name.
  if (!streetName) {
    street = place.name;
  } else {
    if (!streetNumber) {
      streetNumber = '';
    }
    street = (streetName + ' ' + streetNumber).trim();
  }

  return {
    street,
    zip,
    city,
    region,
    country
  };
}

// Parse MySQL DATETIME into object.
export function parseDatetime(datetime: string): {
  date: NgbDate,
  time: NgbTime
} {
  const dateArr = datetime.split(' ')[0].split('-');
  const timeArr = datetime.split(' ')[1].split(':');

  return {
    date: new NgbDate(
      parseInt(dateArr[0], 10),
      parseInt(dateArr[1], 10),
      parseInt(dateArr[2], 10)
    ),
    time: new NgbTime(
      parseInt(timeArr[0], 10),
      parseInt(timeArr[1], 10),
      parseInt(timeArr[2], 10)
    )
  };
}


