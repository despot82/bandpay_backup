"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var StgLogin = (function () {
    function StgLogin(restService) {
        this.restService = restService;
        this.email = '';
        this.password = '';
        this.verified = false;
        this.pages = [];
        this.submitted = false;
        this.result = '';
    }
    StgLogin.prototype.ngOnInit = function () { };
    StgLogin.prototype.onVerify = function () {
        var _this = this;
        this.restService.verifyStgAccount(this.email, this.password)
            .subscribe(function (json) {
            _this.stgId = json.stg_id;
            _this.restService.getStagendPages(_this.stgId)
                .subscribe(function (json) { return _this.pages = json.pages; });
        });
    };
    StgLogin.prototype.onSubmit = function () {
        var _this = this;
        this.restService.postStagendVendor(this.stgId, this.selectedPage)
            .subscribe(function (json) { return _this.result = JSON.stringify(json); });
    };
    return StgLogin;
}());
StgLogin = __decorate([
    core_1.Component({
        selector: 'stg-login',
        templateUrl: 'stg_login.component.html'
    })
], StgLogin);
exports.StgLogin = StgLogin;
