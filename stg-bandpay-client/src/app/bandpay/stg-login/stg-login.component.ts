import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Response, ResponseContentType, Headers } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';

import { StgUser } from '../shared/stg_user';
import { LoginService } from '../shared/login.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RestService } from '../shared/rest.service';

import { MdDialog, MdDialogRef } from '@angular/material';
import { StgLoginDialogComponent } from './stg-login-dialog.component';

import { DatastoreService } from '../shared/datastore.service';

import { TranslateService, TranslatePipe } from 'ng2-translate';
import { IntercommService } from '../shared/intercomm.service';

@Component({
  selector: 'stg-login',
  templateUrl: 'stg-login.component.html',
  styleUrls: ['stg-login.component.scss']
})
export class StgLoginComponent implements OnInit {
  model: StgUser;

  private dialogRef: MdDialogRef<StgLoginDialogComponent>;

  constructor(
    public dialog: MdDialog, 
    private rest: RestService, 
    private login: LoginService, 
    private sanitizer: DomSanitizer,
    
    private datastore: DatastoreService,
    private translate: TranslateService,
    private intercomm: IntercommService
  ) {
    this.model = new StgUser();
    
    translate.use(this.datastore.getValue("language"));
    
    // Subscribe to the selectedLanguage Subject of the intercomm service
    this.intercomm.selectedLanguage.asObservable().subscribe((value: any) => {
        translate.use(value);
        
    });
  }

  // For some reason login.loggedIn cannot be read directly.
  get loggedIn(): boolean {
    return this.login.loggedIn;
  }

  ngOnInit() {
    this.login.onLogin.subscribe(() => this.populateModel());
  }

  onOpenDialog() {
    this.dialogRef = this.dialog.open(StgLoginDialogComponent);
  }

  onLogout() {
    this.login.logout();
    this.model = new StgUser();
  }

  // Populate the model based on the current state of the login service.
  private populateModel(): void {
    this.model = new StgUser();
    this.rest.get(`/vendor/${this.login.userId}?fields=stg_id,first_name,last_name,email`)
      .subscribe(
      (value) => {
        const body = value.json();
        // Default values are for anonymous users.
        this.model.firstName = body.first_name || body.email;
        this.model.lastName = body.last_name || '';
        this.model.email = body.email;
      });
    if (!this.login.anonymous) {
      this.rest.get(
        `/vendor/${this.login.userId}/image`,
        {
          headers: new Headers(),
          responseType: ResponseContentType.Blob
        }
      )
        .subscribe(
        (value) => {
          this.model.imageUrl = this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(value.blob())) as string;
        });
    }
  }

  get debug() {
    const model = {
      firstName: this.model.firstName,
      lastName: this.model.lastName,
      email: this.model.email,
      password: '•'.repeat(this.model.password.length),
      imageUrl: this.model.imageUrl
    };
    return JSON.stringify(model);
  }
}
