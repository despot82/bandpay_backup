import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { StgLoginComponent } from './stg-login.component';
import { StgUser } from '../shared/stg_user';
import { LoginService } from '../shared/login.service';

@Component({
  selector: 'stg-login-dialog',
  templateUrl: 'stg-login-dialog.component.html',
  styleUrls: ['./stg-login-dialog.component.scss']
})
export class StgLoginDialogComponent {
  model: StgUser;

  // Flag controlling the visibility of the sign in failed message.
  authFailed: boolean;

  constructor(public dialogRef: MdDialogRef<StgLoginDialogComponent>, private login: LoginService) {
    this.model = new StgUser();
    this.authFailed = false;
  }

  onLogin() {
    this.login.login({
      email: this.model.email,
      password: this.model.password
    })
      .subscribe(
      () => {
        this.authFailed = false;
        this.dialogRef.close();
      },
      () => {
        this.authFailed = true;
      }
      );
  }
}
