import { Component, OnInit } from '@angular/core';
import { RestService } from '../shared/rest.service';
import 'chance';
import { Slot } from '../shared/slot';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { DatastoreService } from '../shared/datastore.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../shared/login.service';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'test',
  templateUrl: 'test.component.html'
})
export class TestComponent {
  constructor(
    private rest: RestService,
    private login: LoginService,
    private datastore: DatastoreService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  generateRandomDeal() {
    const paypalData = { email: chance.email() };
    const bankData = {
      beneficiary: chance.name(),
      street: chance.address(),
      zip: chance.zip(),
      city: chance.city(),
      country: chance.country(),
      iban: chance.country() + chance.integer({ min: 35, max: 45 })
    };

    const vendorPhone = /\((.+)\) (.+)/.exec(chance.phone());
    let vendor;
    if (chance.bool()) {
      vendor = {
        name: chance.name(),
        email: chance.email(),
        phone_code: vendorPhone[1],
        phone: vendorPhone[2],

        paypal_data: paypalData
      };
    } else {
      vendor = {
        name: chance.name(),
        email: chance.email(),
        phone_code: vendorPhone[1],
        phone: vendorPhone[2],

        bank_data: bankData
      };
    }

    const buyerPhone = /\((.+)\) (.+)/.exec(chance.phone());
    const buyer = {
      first_name: chance.first(),
      last_name: chance.last(),
      email: chance.email(),
      phone_code: buyerPhone[1],
      phone: buyerPhone[2],

      holder: chance.name(),
      street: chance.address(),
      zip: chance.zip(),
      city: chance.city(),
      country: chance.country()
    };
    let profileId: number;
    let buyerId: number;

    this.login.anonymousLogin({ email: vendor.email })
      .flatMap(
      () => this.rest.post(
        `/vendor/${this.login.userId}/profile`,
        vendor
      ))
      .flatMap(
      (value) => {
        profileId = value.json().id;

        return this.rest.post(
          '/buyer',
          buyer
        );
      })
      .flatMap(
      (value) => {
        buyerId = value.json().id;
        const date = chance.date();
        const slot = {
          vendor_id: profileId,
          buyer_id: buyerId,
          start_time: `${date.getFullYear()}-${date.getMonth()}-${date.getDay()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`,
          category: [
            'Background Music',
            'Birthday',
            'Ceremony',
            'Club Event',
            'Corporate Event',
            'Event',
            'Festival',
            'Party',
            'Party for Kids',
            'Religious Ceremony',
            'Seasonal Event',
            'Wedding'
          ][chance.integer({ min: 0, max: 11 })],
          duration: chance.integer({ min: 0, max: 8 }) * 3600 + chance.integer({ min: 0, max: 60 }) * 60,
          street: chance.address(),
          zip: chance.zip(),
          city: chance.city(),
          country: chance.country(),
          guests_number: chance.integer({ min: 1, max: 150 }),
          audio_system: chance.bool(),
          lighting_system: chance.bool(),
          catering_included: chance.integer({ min: 1, max: 20 }),
          accomodation_included: chance.integer({ min: 1, max: 20 }),
          cancellation_policy: ['standard', 'moderate'][chance.integer({ min: 0, max: 1 })],
          transaction: {
            amount: chance.floating({ min: 1, max: 800 }),
            currency: ['chf', 'eur'][chance.integer({ min: 0, max: 1 })]
          }
        };
        return this.rest.post(
          '/slot',
          slot
        );
      })
      .subscribe(
      (value) => {
        const slotId = value.json().id;
        this.datastore.setValue('slotId', slotId);
        this.datastore.setValue('profileId', profileId);
        this.datastore.setValue('buyerId', buyerId);

        this.router.navigate(['artist', 'summary']);
      });
  }
}
