import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostFinanceButtonComponent } from './postfinance-button.component';

describe('PostFinanceFormComponent', () => {
  let component: PostFinanceButtonComponent;
  let fixture: ComponentFixture<PostFinanceButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostFinanceButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostFinanceButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
