import { Component, OnInit, Host, Input, Optional, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { RestService } from '../../../shared/rest.service';
import { environment } from '../../../../../environments/bp-environment';
import * as CryptoJS from 'crypto-js';
import { CheckoutComponent } from '../../checkout/checkout.component';
import { ActivatedRoute } from '@angular/router';
import { parseDatetime } from '../../../shared/parsing-helper';
import { Slot } from '../../../shared/slot';
import { Buyer } from '../../../shared/buyer';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'postfinance-button',
  templateUrl: './postfinance-button.component.html',
  styleUrls: ['./postfinance-button.component.scss']
})
export class PostFinanceButtonComponent implements OnInit, DoCheck {
  readonly paymentMethods = environment.postFinance.paymentMethods;

  formParams: Array<{ name: string, value: string }>;

  url: string;
  passphrase: string;

  checkoutToken: string;

  slotId: number;
  slot: Slot;

  buyer: Buyer;

  constructor(
    @Optional()
    private checkout: CheckoutComponent,
    private route: ActivatedRoute,
    private rest: RestService,
    private sanitizer: DomSanitizer
  ) {
    this.formParams = null;
    this.url = environment.postFinance.url;
    this.passphrase = environment.postFinance.shaIn;
    this.checkoutToken = null;
    this.slotId = null;
    this.slot = new Slot();
    this.buyer = new Buyer();
    
  }

  ngOnInit() {
    if (this.checkout) {
      this.checkoutToken = this.checkout.token;
    } else {
      this.route.params
        .subscribe((params) => {
          this.checkoutToken = params.token;
        });
    }

    // Get all the data from the API to fill the form parameters.
    this.rest.get(`/checkout/${this.checkoutToken}`)
      .flatMap((value) => {
        this.slotId = value.json().slot_id;
        return this.rest.get(`/slot/${this.slotId}?fields=id,buyer_id&embed=transaction`);
      })
      .flatMap((value) => {
        const body = value.json();

        this.slot.transaction.amount = body.transaction.amount;
        this.slot.transaction.currency = body.transaction.currency;
        this.slot.transaction.status = body.transaction.status;

        return this.rest.get(`/buyer/${body.buyer_id}`);
      })
      .subscribe(
      (value) => {
        const body = value.json();

        this.buyer.firstName = body.first_name;
        this.buyer.lastName = body.last_name;
        this.buyer.email = body.email;
        this.buyer.phoneCode = body.phone_code;
        this.buyer.phone = body.phone;

        this.buyer.holder = body.holder;
        this.buyer.street = body.street;
        this.buyer.zip = body.zip;
        this.buyer.city = body.city;
        this.buyer.country = body.country;
      });
  }

  ngDoCheck(): void {
    this.populateFormParams();
  }

  getPaymentMethodCaption(pm: string): SafeHtml {
    const caption = this.slot.transaction.paymentMethods.find((value) => value.pm === pm).caption;
    return this.sanitizer.bypassSecurityTrustHtml(caption);
  }

  createSHASign() {
    if (this.formParams) {
      // Sort parameters alphabetically.
      const sortedParams = this.formParams
        .sort((a, b) => {
          return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
        });

      const stringToHash = sortedParams
        // null values not included in the sign.
        .filter((param) => param.value != null)
        .map((param) => `${param.name}=${param.value}${this.passphrase}`)
        .reduce((prev, cur) => prev.concat(cur));
      
      const hash = CryptoJS.SHA1(stringToHash).toString();
      return hash;
    }
  }

  private populateFormParams() {
    if (!this.slotId || this.slot.transaction.paymentMethod === 'invoice') {
      return;
    }
    const slot = this.slot;
    const buyer = this.buyer;
    const paymentMethods = this.paymentMethods;
    this.formParams = [
      { name: 'PSPID', value: 'stagendTEST' },
      { name: 'ORDERID', value: this.slotId.toString() },
      { name: 'AMOUNT', value: (slot.transaction.amount * 100).toString() },
      { name: 'CURRENCY', value: slot.transaction.currency.toUpperCase() },
      { name: 'LANGUAGE', value: 'en_US' },
      { name: 'CN', value: `${buyer.firstName} ${buyer.lastName}` },
      { name: 'EMAIL', value: buyer.email },
      { name: 'OWNERZIP', value: buyer.zip },
      { name: 'OWNERADDRESS', value: buyer.street },
      { name: 'OWNERCTY', value: buyer.country },
      { name: 'OWNERTOWN', value: buyer.city },
      { name: 'OWNERTELNO', value: `+${buyer.phoneCode} ${buyer.phone}` },

      { name: 'ACCEPTURL', value: `${window.location.origin}/checkout/${this.checkoutToken}/success` },
      { name: 'DECLINEURL', value: `${window.location.origin}/checkout/${this.checkoutToken}/declined` },
      { name: 'EXCEPTIONURL', value: `${window.location.origin}/checkout/${this.checkoutToken}/error` },
      { name: 'CANCELURL', value: `${window.location.origin}/checkout/${this.checkoutToken}/cancelled` },
      { name: 'BACKURL', value: `${window.location.origin}/checkout/${this.checkoutToken}` },

      { name: 'PM', value: paymentMethods.find((pm) => pm.brand === this.slot.transaction.paymentMethod).pm },
      { name: 'BRAND', value: paymentMethods.find((pm) => pm.brand === this.slot.transaction.paymentMethod).brand },
    ];
  }
}
