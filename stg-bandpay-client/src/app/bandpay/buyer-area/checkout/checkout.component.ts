import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router, RoutesRecognized } from '@angular/router';
import { Response } from '@angular/http';
import { RestService } from '../../shared/rest.service';
import { Transaction } from '../../shared/slot';

@Component({
  selector: 'checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  token: string;
  slotId: number;

  constructor(private route: ActivatedRoute, private rest: RestService, private router: Router) { }

  ngOnInit() {
      this.route.params
        .flatMap((params) => {
            this.token = params.token;
            return this.rest.get(`/checkout/${this.token}`);
        })
        .subscribe((value) => {
            this.slotId = value.json().slot_id;
        
            //TO DO: check MFG credit capability of the customer. If invoice amount is bigger then the credit, or
            //       the amount is bigger than 4000, don't display invoice as a payment option. Otherwise display it.
        
            //TO DO: when the aboe(last) call is finished subscribe to tis response. Get the virtual card number
            //       and put it inside the hidden field to be sent then if the invoice is selected, to pay.
        
            // TODO: Redirect to checkout-callback if the payment has already been confirmed.
        },
        (error) => {
            if (error instanceof Response) {
                if (error.status === 401) {
                    this.router.navigate(['checkout', this.token, 'error']/*, {replaceUrl: true}*/);
                } else if (error.status === 410) {
                    this.router.navigate(['checkout', this.token, 'error']/*, {replaceUrl: true}*/);
                }
            }
        });
  }
}
