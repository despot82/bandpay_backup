/*
 * TODO: Add landing page before checkout page.
 * TODO: Invoice and PDF also in checkout-callback only if PM = invoice.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyerAreaRoutingModule } from './buyer-area-routing.module';
import { CheckoutComponent } from './checkout/checkout.component';
import { CheckoutCallbackComponent } from './checkout-callback/checkout-callback.component';
import { SharedModule } from '../shared/shared.module';
import { PostFinanceButtonComponent } from './checkout/postfinance-button/postfinance-button.component';

@NgModule({
  imports: [
    BuyerAreaRoutingModule,
    CommonModule,
    SharedModule,
  ],
  declarations: [
    CheckoutComponent,
    CheckoutCallbackComponent,
    PostFinanceButtonComponent
  ]
})
export class BuyerAreaModule { }
