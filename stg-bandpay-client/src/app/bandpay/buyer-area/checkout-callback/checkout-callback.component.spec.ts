import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutCallbackComponent } from './checkout-callback.component';

describe('CheckoutCallbackComponent', () => {
  let component: CheckoutCallbackComponent;
  let fixture: ComponentFixture<CheckoutCallbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutCallbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutCallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
