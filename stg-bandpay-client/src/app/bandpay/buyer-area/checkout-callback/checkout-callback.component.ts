import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RestService } from '../../shared/rest.service';
import { Transaction } from '../../shared/slot';
import { environment } from '../../../../environments/bp-environment';
import * as CryptoJS from 'crypto-js';
import 'rxjs/add/observable/throw';
import { Response } from '@angular/http';
import { Location } from '@angular/common';

@Component({
  selector: 'checkout-callback',
  templateUrl: './checkout-callback.component.html',
  styleUrls: ['./checkout-callback.component.css']
})
export class CheckoutCallbackComponent implements OnInit {
  readonly paymentMethods: {
    [key: string]: string
  } = {
    'PostFinance+Card': 'postfinance_card',
    'PostFinance+e-finance': 'e_finance',
    'CreditCard': 'credit_card',
    'PAYPAL': 'paypal'
  };

  token: string;
  status: string;
  passphrase: string;

  queryParams: Params;

  slotId: number;

  transaction: Transaction;

  constructor(private route: ActivatedRoute, private router: Router, private rest: RestService, private location: Location) {
    this.token = null;
    this.status = null;
    this.passphrase = environment.postFinance.shaOut;
    this.queryParams = null;
    this.slotId = null;
    this.transaction = new Transaction();
  }

  ngOnInit() {
    
    this.route.params
      .flatMap((params) => {
        this.token = params.token;
        this.status = params.status;
        return this.route.queryParams;
      })
      .subscribe((queryParams) => {
        this.queryParams = queryParams;
        switch (this.status) {
          case 'success':
            if (this.queryParams.PM !== 'invoice') {
                if (!this.checkShaOut(this.queryParams)) {
                    // Reset the status.
                    this.status = null;
                    // Handle signature error.
                    this.router.navigate(['checkout', this.token, 'error']/*, {replaceUrl: true}*/);
                    return;
                } else {
                    this.handleSuccess();
                }
            } else {
                //process invoice (MFG group) payment
                this.handleMFGCallback();
            }
            
            break;
          case 'declined':
            this.handleDeclined();
            break;
          case 'error':
            this.handleError();
            break;
          case 'cancelled':
            this.handleCancelled();
            break;
        }
      });
  }
 
  handleSuccess() {
    const params = this.queryParams;
    let paramsArr = {};
    for (const key in params) {
      
        // Convert + sign to space. Angular decodes spaces in query parameters as '+'.
        const value = (params[key] as string).replace(/\+/g, ' ');
        paramsArr[key] = value;
      
    }
    
    console.log(this.queryParams);
    console.log(paramsArr);
  
    this.rest.patch(
      `/checkout/${this.token}/success`,
      paramsArr
      
    )
    .subscribe();
  }
  
  handleMFGCallback() {
      alert("MFG callback occurs");
  }

  handleDeclined() {
    this.router.navigate(['checkout', this.token]);
  }

  handleError() {

  }

  handleCancelled() {

  }

  private checkShaOut(params: Params): boolean {
    if (params.SHASIGN === undefined) {
      return false;
    }
    const paramsArr: Array<{ name: string, value: string }> = [];
    for (const key in params) {
      // Exclude SHASIGN and empty parameters.
      if (key !== 'SHASIGN' && params[key] !== '') {
        // Convert + sign to space. Angular decodes spaces in query parameters as '+'.
        const value = (params[key] as string).replace(/\+/g, ' ');
        paramsArr.push({ name: key.toUpperCase(), value });
      }
    }

    // Sort parameters alphabetically.
    const sortedParams = paramsArr.sort((a, b) => {
      return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
    });

    const stringToHash = sortedParams
      .map((param) => `${param.name}=${param.value}${this.passphrase}`)
      .reduce((prev, cur) => prev.concat(cur));
    
    //alert(stringToHash);
    
    const hash = CryptoJS.SHA1(stringToHash).toString().toUpperCase();
    
    //alert("is hash equal to params shasign:" + (hash === params['SHASIGN']) );

    return hash === params['SHASIGN'];
  }

}
