import { Routes, RouterModule } from '@angular/router';
import { CheckoutComponent } from './checkout/checkout.component';
import { CheckoutCallbackComponent } from './checkout-callback/checkout-callback.component';

const BuyerAreaRoutes: Routes = [
  { path: ':token', component: CheckoutComponent },
  { path: ':token/:status', component: CheckoutCallbackComponent },
];

export const BuyerAreaRoutingModule = RouterModule.forChild(BuyerAreaRoutes);
