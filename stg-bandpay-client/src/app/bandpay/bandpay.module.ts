import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { Ng2Webstorage } from 'ngx-webstorage/dist/app';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CommonModule } from '@angular/common';
import { StgLoginComponent } from './stg-login/stg-login.component';
import { FormsModule } from '@angular/forms';
import { RestService } from './shared/rest.service';
import { IntercommService } from './shared/intercomm.service';
import { JwtHttp } from './shared/jwt-http.service';
import { LoginService } from './shared/login.service';
import { MaterialModule } from '@angular/material';
import { StgLoginDialogComponent } from './stg-login/stg-login-dialog.component';
import { TestComponent } from './test/test.component';
import { DatastoreService } from './shared/datastore.service';
import { TranslateModule } from 'ng2-translate';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    TranslateModule,

    AngularFontAwesomeModule,
  ],
  declarations: [
    StgLoginComponent,
    StgLoginDialogComponent,
    TestComponent
  ],
  entryComponents: [
    StgLoginDialogComponent
  ],
  exports: [
    StgLoginComponent,
    TestComponent,
    TranslateModule
  ],
  providers: [
    JwtHttp,
    RestService,
    IntercommService,
    LoginService,
    DatastoreService
  ]
})

export class BandpayModule { }
