import { RouterModule, Routes } from '@angular/router';

import { HomepageComponent } from './homepage/homepage.component';
import { TermsComponent } from './terms/terms.component';
import { SlotFormComponent } from './slot-form/slot-form.component';
import { BuyerFormComponent } from './buyer-form/buyer-form.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';
import { SummaryComponent } from './summary/summary.component';


const VendorAreaRoutes: Routes = [
  { path: 'terms', component: TermsComponent },
  { path: 'event', component: SlotFormComponent },
  { path: 'organizer', component: BuyerFormComponent },
  { path: 'profile', component: ProfileFormComponent },
  { path: 'summary', component: SummaryComponent }
  
];

export const VendorAreaRoutingModule = RouterModule.forChild(VendorAreaRoutes);
