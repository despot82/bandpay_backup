import { Component, OnInit, ViewChild, Type, ElementRef, ContentChildren, AfterContentInit, QueryList, Input } from '@angular/core';
import { ActivatedRoute, RouterOutlet, Router, OutletContext, ChildrenOutletContexts } from '@angular/router';
import { StepDirective } from './step.directive';
import { DatastoreService } from '../../shared/datastore.service';
import { environment } from '../../../../environments/environment.prod';

@Component({
  selector: 'steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class StepsComponent implements AfterContentInit {
  @Input()
  showIndexes: boolean;

  @ContentChildren(StepDirective)
  steps: QueryList<StepDirective>;

  components: Array<Type<any>>;

  constructor(
    private outletContexts: ChildrenOutletContexts,
    private router: Router,
    private route: ActivatedRoute,
    private datastore: DatastoreService
  ){
    this.showIndexes = false;
  }

  ngAfterContentInit() {
    this.components = this.steps.map((step) => {
      return step.component;
    });
  }

  get activeComponent(): Type<any> {
    return this.outletContexts.getContext('primary').route.component as Type<any>;
  }

  isActive(component: Type<any>): boolean {
    return component === this.activeComponent;
  }

  getCaption(component: Type<any>): string {
    const index = this.showIndexes !== false ? (this.components.indexOf(component) + 1) + '. ' : '';
    return index + this.steps.find((step) => step.component === component).content;
  }
  
  goToStep(stepNumberToGoTo:number) {
      //alert("hey, the steps component , something is clicked inside of it!" + stepNumberToGoTo);
      
      if(stepNumberToGoTo == 0)
      this.router.navigate(['/artist/event']);
      
      if(stepNumberToGoTo == 1)
      this.router.navigate(['/artist/organizer']);
      
      if(stepNumberToGoTo == 2)
      this.router.navigate(['/artist/profile']);
      
      if(stepNumberToGoTo == 3) {
      
          if(!this.datastore.getValue('slotId')) {
            if(environment.dev) {
              this.router.navigate(['artist/summary']);
            } else { 
               return;
            }
          } else {
            this.router.navigate(['artist/summary']);
          }
          
      }
      
  }

}
