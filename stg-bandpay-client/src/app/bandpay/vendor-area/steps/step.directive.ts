import { Directive, Type, Input, ViewContainerRef, ComponentFactoryResolver, ComponentFactory, OnInit, ElementRef } from '@angular/core';
 
@Directive({
  selector: 'step'
})
export class StepDirective implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('component')
  componentName: string;

  component: Type<any>;

  constructor(private element: ElementRef, private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
    const components = Array.from((this.resolver['_factories'] as Map<Type<any>, ComponentFactory<any>>).keys());
    this.component = components.find((comp) => comp.name === this.componentName);
  }

  get content(): string {
    return this.element.nativeElement.innerHTML;
  }
}
