import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { VendorProfile, BankData, PaypalData } from '../../shared/vendor_profile';
import { RestService } from '../../shared/rest.service';
import { DatastoreService } from '../../shared/datastore.service';
import { Slot } from '../../shared/slot';
import { Buyer } from '../../shared/buyer';
import { parseDatetime } from '../../shared/parsing-helper';
import { Headers, ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment.prod';

import { TranslateService, TranslatePipe } from 'ng2-translate';
import { IntercommService } from '../../shared/intercomm.service';

@Component({
  selector: 'summary',
  templateUrl: 'summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  slotId: number;
  
  pdfUrl: string;

  @Output() emitPdfUrl = new EventEmitter();
  
  submitted: boolean;

  constructor(
    private rest: RestService, 
    private datastore: DatastoreService, 
    private router: Router,
    private intercomm: IntercommService,
    private translate: TranslateService
  ) {
    this.slotId = this.datastore.getValue('slotId');
    this.submitted = false;
    
    translate.use(this.datastore.getValue("language"));
    
    // Subscribe to the selectedLanguage Subject of the intercomm service
    this.intercomm.selectedLanguage.asObservable().subscribe((value: any) => {
        translate.use(value);
    });
  }

  ngOnInit() {
    if (!this.slotId) {
      
      if(environment.dev) {
        this.slotId = 309; 
        this.datastore.setValue('slotId', this.slotId);
      } else {
        this.router.navigate(['']);
      }
    }
  }
 
  onSubmit($event) {
  
    this.submitted = true;
    this.rest.post(
      '/checkout',
      { slot_id: this.slotId }
    )
    .subscribe((value) => {
      this.datastore.setValue('slotId', null);
      this.datastore.setValue('buyerId', null);
      this.datastore.setValue('profileId', null);
    });
  }
   
  getInvoicePdfUrl($event) {
    
    //alert("in summary - event is: " + $event);
    this.pdfUrl = $event;
  }
  
  goToPdfUrl(){
    open(this.pdfUrl);
  }
  

}
