import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { DatastoreService } from '../shared/datastore.service';
import { IntercommService } from '../shared/intercomm.service';

@Component({
  selector: 'bp-vendor-area',
  styles: [],
  templateUrl: './vendor-area.component.html'
})
export class VendorAreaComponent {
    constructor(
      private translate: TranslateService,
      private datastore: DatastoreService,
      private intercomm: IntercommService
    ) {
      
      translate.setDefaultLang('en');
      if(!this.datastore.getValue('language')) {
        this.datastore.setValue('language', 'en');
      }
      
      translate.use(this.datastore.getValue('language'));
      
      this.intercomm.selectedLanguage.next(this.datastore.getValue('language'));
      
    }
}
