import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Buyer } from '../../shared/buyer';
import { RestService } from '../../shared/rest.service';
import { Http } from '@angular/http';
import { DatastoreService } from '../../shared/datastore.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../shared/login.service';
import 'rxjs/add/operator/mergeMap';
import { TranslateService, TranslatePipe } from 'ng2-translate';
import { IntercommService } from '../../shared/intercomm.service';

@Component({
  selector: 'buyer-form',
  templateUrl: 'buyer-form.component.html'
})
export class BuyerFormComponent implements OnInit {
  model: Buyer;

  private slotId: number;
  private buyerId: number;

  constructor(
    private rest: RestService,
    private login: LoginService,
    private http: Http,
    private datastore: DatastoreService,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private intercomm: IntercommService
  ) {
    
    this.model = new Buyer();
    this.slotId = this.datastore.getValue('slotId');
    this.buyerId = this.datastore.getValue('buyerId');
    
    translate.use(this.datastore.getValue("language"));
    
    // Subscribe to the selectedLanguage Subject of the intercomm service
    this.intercomm.selectedLanguage.asObservable().subscribe((value: any) => {
        translate.use(value);
        
    });
    
    
  }

  ngOnInit() {
    
    if(!this.buyerId) {
    } else {
        this.populateModel();
    }
   
  }

  get debug() {
    return JSON.stringify(this.model);
  }

  onSubmit() {
    
    this.rest.post(
      '/buyer',
      {
        first_name: this.model.firstName,
        last_name: this.model.lastName,
        company: this.model.company,
        email: this.model.email,
        phone_code: this.model.phoneCode,
        phone: this.model.phone,

        holder: this.model.holder,
        street: this.model.street,
        zip: this.model.zip,
        city: this.model.city,
        country: this.model.country
      }).flatMap(
      (value) => {
        const buyerId = value.json().id;
        this.datastore.setValue('buyerId', buyerId);
        this.router.navigate(['profile'], { relativeTo: this.route.parent });
        return this.rest.patch(
          `/slot/${this.slotId}`,
          { buyer_id: buyerId }
        );
      })
      .subscribe();
  }

  private populateModel() {
      this.rest.get(`/buyer/${this.buyerId}?fields=first_name,last_name,company,email,phone_code,phone,holder,street,zip,city,country`)
      .subscribe(
      (value) => {
        const body = value.json();
        this.model.firstName = body.first_name;
        this.model.lastName = body.last_name;
        this.model.company = body.company;
        this.model.email = body.email;
        this.model.phoneCode = body.phone_code;
        this.model.phone = body.phone;
        this.model.holder = body.holder;
        
        this.model.street = body.street;
        this.model.zip = body.zip;
        this.model.city = body.city;
        this.model.country = body.country;
        
        this.model.address = body.street + ", " + body.zip + ", " + body.city + ", " + body.country;
        
      });
  
  }
}
