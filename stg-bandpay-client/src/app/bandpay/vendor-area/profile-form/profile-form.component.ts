import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, Input, isDevMode, AfterViewChecked, DoCheck } from '@angular/core';
import { RestService } from '../../shared/rest.service';
import { LoginService } from '../../shared/login.service';
import { VendorProfile, PaypalData, BankData } from '../../shared/vendor_profile';
import { Http } from '@angular/http';
import { parsePlace } from '../../shared/parsing-helper';
import { NgForm } from '@angular/forms';
import { DatastoreService } from '../../shared/datastore.service';
import { JwtHttp } from '../../shared/jwt-http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MdSelectChange } from '@angular/material';
import { TranslateService, TranslatePipe } from 'ng2-translate';
import { IntercommService } from '../../shared/intercomm.service';

@Component({
  selector: 'profile-form',
  templateUrl: 'profile-form.component.html'
})
export class ProfileFormComponent implements OnInit {
  profiles: Array<{
    id: number,
    name: string
  }>;
  profileId: number;

  model: VendorProfile;

  editing: boolean;

  private isNew: boolean;

  // Used on submission.
  private slotId: number;

  constructor(
    public login: LoginService,
    private rest: RestService,
    private http: Http,
    private datastore: DatastoreService,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private intercomm: IntercommService
  ) {
    
    this.profiles = [];
    this.profileId = this.datastore.getValue('profileId');
    this.model = new VendorProfile();
    this.editing = !login.loggedIn;
    this.isNew = !login.loggedIn;

    this.slotId = this.datastore.getValue('slotId');
    
    translate.use(this.datastore.getValue("language"));
    
    // Subscribe to the selectedLanguage Subject of the intercomm service
    this.intercomm.selectedLanguage.asObservable().subscribe((value: any) => {
        translate.use(value);
    });
  }

  ngOnInit() {
     
    if (!this.slotId) {
      if (!isDevMode()) {
        this.router.navigate(['']);
      }
    }
    if (this.profileId) {
      this.populateModel();
    }
    this.login.onLogin.subscribe(() => {
      this.populateProfileSelector();
      this.editing = false; 
    });
    this.login.onLogout.subscribe(() => {
      this.model = new VendorProfile();
      this.profileId = null;
      this.editing = true;
    });
  }

  onNew() {
    this.model = new VendorProfile();
    this.editing = true;
    this.isNew = true;
  }

  onEdit() {
    this.editing = true;
  }

  // I decided to avoid the problem of erasing data when changing banking type by using interfaces.
  // onBankingTypeChanged(event: MdSelectChange) {
  //   console.log(event, this.model.bankingType);
  //   // If the banking information is empty change it without warning.
  //   if (this.model.banking.isUntouched()) {
  //     this.model.banking = event.value;
  //     return;
  //   }
  //   // Prompt the user before erasing banking data.
  //   if (confirm('This will erase your current data. Are you sure?')) {
  //     this.model.bankingType = event.value;
  //   } else {
  //     // Revert change to the previously selected option.
  //     event.source.writeValue(this.model.bankingType);
  //   }
  // }

  onSave() {
    this.saveChanges();
    this.editing = false;
  }
 
  onCancel() {
    this.populateModel();
    this.editing = false;
  }

  onSubmit() {
    this.rest.patch(
      `/slot/${this.slotId}`,
      { vendor_id: this.profileId }
    )
    .subscribe();

    this.datastore.setValue('profileId', this.profileId);
    this.router.navigate(['summary'], { relativeTo: this.route.parent });
  }

  private populateProfileSelector(): void {
    // Populate
    this.rest.get(`/vendor/${this.login.userId}/profile?fields=name,id`)
      .subscribe(
      (value) => {
        const body = value.json() as any[];
        this.profiles = body.map((profile) => {
          return {
            id: profile.id,
            name: profile.name
          };
        });
        // Autoselect the first and only profile if anonymous user.
        if (this.login.anonymous) {
          this.profileId = this.profiles[0].id;
          this.populateModel();
        }
      });
  }

  private populateModel() {
    
    this.rest.get(`/vendor/${this.login.userId}/profile?search&id=${this.profileId}&embed=banking`)
      .subscribe(
      (value) => {
        const body = value.json()[0];
        this.model.name = body.name;
        this.model.email = body.email;
        this.model.phoneCode = body.phone_code;
        this.model.phone = body.phone;
        if (body.paypal_data) {
          this.model.bankingType = 'paypal';
          (this.model.banking as PaypalData).email = body.paypal_data.email;
        } else if (body.bank_data) {
          this.model.bankingType = 'bank';
          const bankData = this.model.banking as BankData;
          bankData.beneficiary = body.bank_data.beneficiary;
          bankData.street = body.bank_data.street;
          bankData.zip = body.bank_data.zip;
          bankData.city = body.bank_data.city;
          bankData.iban = body.bank_data.iban;
        }
      }); //end (value) anonymouse function, end subscribe bracket
  }

  private populateAddress(place: any): void {
    const parsed = parsePlace(place);
    (this.model.banking as BankData).street = parsed.street;
    (this.model.banking as BankData).zip = parsed.zip;
    (this.model.banking as BankData).city = parsed.city;
    (this.model.banking as BankData).country = parsed.country;
  }

  private saveChanges() {
    let body: any;
    if (this.model.bankingType === 'paypal') {
      body = {
        name: this.model.name,
        email: this.model.email,
        phone_code: this.model.phoneCode,
        phone: this.model.phone,
        paypal_data: this.model.banking
      };
    } else if (this.model.bankingType === 'bank') {
      body = {
        name: this.model.name,
        email: this.model.email,
        phone_code: this.model.phoneCode,
        phone: this.model.phone,
        bank_data: this.model.banking
      };
    }

    // If not logged in POST new "anonymous" user with the available information.
    if (!this.login.loggedIn) {
      this.login.anonymousLogin({ email: this.model.email })
        .flatMap(
        () => this.rest.post(
          `/vendor/${this.login.userId}/profile`,
          body
        ))
        .subscribe(
        (value) => {
          this.profileId = value.json().id;
          this.onSubmit();
        });
    } else {
      if (this.isNew) {
        this.rest.post(
          `/vendor/${this.login.userId}/profile`,
          body
        )
          .subscribe(
          // Update profiles information.
          (value) => {
            this.profileId = value.json().id;
            this.populateProfileSelector();
          });
      } else {
        this.rest.patch(
          `/profile/${this.profileId}`,
          body
        )
          .subscribe(
          // Update profiles information.
          () => this.populateProfileSelector()
          );
      }
    }
  }

  get debug() {
    return this.model;
  }

  get debugLogin() {
    const login = {
      loggedIn: this.login.loggedIn,
      anonymous: this.login.anonymous,
      userId: this.login.userId
    };
    return JSON.stringify(login);
  }
}
