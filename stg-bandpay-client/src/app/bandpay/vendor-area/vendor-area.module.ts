import { NgModule } from '@angular/core';
import { VendorAreaComponent } from './vendor-area.component';
import { VendorAreaRoutingModule } from './vendor-area-routing.module';

import { HomepageComponent } from './homepage/homepage.component';
import { TermsComponent } from './terms/terms.component';
import { UITestimonialsComponent } from './homepage/testimonials.component';
import { SlotFormComponent } from './slot-form/slot-form.component';
import { BuyerFormComponent } from './buyer-form/buyer-form.component';
import { ProfileFormComponent } from './profile-form/profile-form.component';
import { SummaryComponent } from './summary/summary.component';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { Ng2Webstorage } from 'ngx-webstorage';
import { AgmCoreModule } from '@agm/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RestService } from '../shared/rest.service';
import { JwtHttp } from '../shared/jwt-http.service';
import { LoginService } from '../shared/login.service';
import { DatastoreService } from '../shared/datastore.service';
import { GooglePlacesAutocompleteDirective } from '../shared/google-places-autocomplete.directive';
import { SharedModule } from '../shared/shared.module';
import { CommonModule, JsonPipe } from '@angular/common';
import { MaterialModule, DateAdapter } from '@angular/material';
import { MomentDateAdapter, MOMENT_DATE_FORMATS } from '../shared/moment-date-adapter';
import { MD_DATE_FORMATS, MD_NATIVE_DATE_FORMATS } from '@angular/material';
import { StepsComponent } from "./steps/steps.component";
import { StepDirective } from "./steps/step.directive";


@NgModule({
  imports: [
    VendorAreaRoutingModule,
    SharedModule,

    FormsModule,
    CommonModule,
    MaterialModule,

    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA-MvpqfnVZqzuOMU6-oS_wF-4Kn_SHDSU',
      libraries: ['places']
    }),
    Ng2Webstorage,
    AngularFontAwesomeModule
    
  ],
  declarations: [
    VendorAreaComponent,
    
    HomepageComponent,
    TermsComponent,
    UITestimonialsComponent,
    SlotFormComponent,
    BuyerFormComponent,
    ProfileFormComponent,
    SummaryComponent,
    
    StepsComponent,
    StepDirective,
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter },
    { provide: MD_DATE_FORMATS, useValue: MOMENT_DATE_FORMATS },
  ]
})
export class VendorAreaModule { }
