import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { RestService } from '../../shared/rest.service';
import { LoginService } from '../../shared/login.service';
import { Slot } from '../../shared/slot';
import { NgForm, FormGroup } from '@angular/forms';
import { parsePlace } from '../../shared/parsing-helper';
import { DatastoreService } from '../../shared/datastore.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MdCheckbox } from '@angular/material';
import { TranslateService, TranslatePipe } from 'ng2-translate';
import { IntercommService } from '../../shared/intercomm.service';

@Component({
  selector: 'slot-form',
  templateUrl: './slot-form.component.html',
  styleUrls: ['./slot-form.component.scss'],
})
export class SlotFormComponent implements OnInit {
  model: Slot;
  private slotId: number;
  nextButtonText: string;
  
  @ViewChild('isAccomodationIncludedCheckbox') isAccomodationIncludedCheckbox:MdCheckbox;
  @ViewChild('isCateringIncludedCheckbox') isCateringIncludedCheckbox:MdCheckbox;

  constructor(
    private rest: RestService,
    private login: LoginService,
    private datastore: DatastoreService,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private intercomm: IntercommService
  ) {
      this.model = new Slot();
      this.slotId = this.datastore.getValue('slotId');
      this.nextButtonText = "Finish";
      
      translate.use(this.datastore.getValue("language"));
      
      // Subscribe to the selectedLanguage Subject of the intercomm service
      this.intercomm.selectedLanguage.asObservable().subscribe((value: any) => {
        translate.use(value);
      });
  }
  
  ngOnInit() {
      if(!this.slotId) {
      } else {
          this.populateModel(); // If slotId is previously set somewhere, populate the model
      }
      
  }

  get debug() {
    return this.model;
  }

  onCateringChanged(value) {
    
  }

  onAccomodationChanged(value) {
    
  }

  onSubmit() {
    if(!this.isCateringIncludedCheckbox.checked || this.model.cateringIncluded < 1) this.model.cateringIncluded = null;
    if(!this.isAccomodationIncludedCheckbox.checked || this.model.accomodationIncluded < 1) this.model.accomodationIncluded = null;
    
    this.rest.post(
      '/slot',
      this.createBody()
    )
    .subscribe(
    (value) => {
        
        const slotId = value.json().id;
        this.datastore.setValue('slotId', slotId);
        this.router.navigate(['organizer'], { relativeTo: this.route.parent });
        
    });
    
  }
  
  //Prepare for POST-ing
  private createBody(): {} {
    const body = {
      buyer_id: this.datastore.getValue('buyerId'),
      vendor_id: this.datastore.getValue('profileId'),
      start_time: this.model.startTime.format('YYYY-MM-DD HH:mm:ss'),
      category: this.model.category,
      duration: this.model.duration,
      street: this.model.street,
      zip: this.model.zip,
      city: this.model.city,
      country: this.model.country,
      guests_number: this.model.guestsNumber,
      audio_system: this.model.audioSystem,
      lighting_system: this.model.lightingSystem,
      catering_included: this.model.cateringIncluded,
      accomodation_included: this.model.accomodationIncluded,
      cancellation_policy: this.model.cancellationPolicy,
      transaction: {
        amount: this.model.transaction.amount,
        currency: this.model.transaction.currency
      }
    };
    return body;
  }
      
  // Populate model for displaying it on the template    
  private populateModel() {
      //alert("in populate model in slot form component");
      this.rest.get(`/slot/${this.slotId}?fields=start_time,category,duration,street,zip,city,country,guests_number,audio_system,lighting_system,accomodation_included,catering_included&embed=transaction`)
      .subscribe(
      (value) => {
        const body = value.json();
        
        this.model.street = body.street;
        this.model.zip = body.zip;
        this.model.city = body.city;
        this.model.country = body.country;
        this.model.setStartTime(body.start_time);
        this.model.category = body.category;
        this.model.duration = body.duration;
        this.model.guestsNumber = body.guests_number;
        this.model.audioSystem = body.audio_system == 1 ? true : false;
        this.model.lightingSystem = body.lighting_system == 1 ? true : false;
        
        //alert("receive body  accomodation_included is: " + body.accomodation_included);
        
        this.isCateringIncludedCheckbox.checked = body.catering_included && body.catering_included > 0 ? true : false;
        this.isAccomodationIncludedCheckbox.checked = body.accomodation_included && body.accomodation_included > 0 ? true : false;
        
        
        this.model.cateringIncluded = body.catering_included;
        this.model.accomodationIncluded = body.accomodation_included;
        
        //alert("body dot transaction amount is: " + body.transaction.amount);
        //alert("body dot transaction currency is: " + body.transaction.currency);
        
        this.model.transaction.amount = body.transaction.amount;
        this.model.transaction.currency = body.transaction.currency;
       
        
      });
  
  }
}
