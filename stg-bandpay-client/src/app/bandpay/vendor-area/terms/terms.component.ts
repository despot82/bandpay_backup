import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css'],
})
export class TermsComponent implements OnInit {
  
  submitted;
  
  constructor(private router: Router,
  ) {
      
  }
  
  ngOnInit() {
     
  }
  
  go() {
    this.router.navigate(['/artist/event']);
  }
  
  onSubmit() { 
    
    event.preventDefault();
    this.submitted = true;
  }

}
