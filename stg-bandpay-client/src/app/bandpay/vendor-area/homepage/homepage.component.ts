import { Component, OnInit } from '@angular/core';
import { UITestimonialsComponent } from './testimonials.component';
import { Router } from '@angular/router';
import { DatastoreService } from '../../shared/datastore.service';

import { TranslateService, TranslatePipe } from 'ng2-translate';
import { IntercommService } from '../../shared/intercomm.service';

@Component({
  selector: 'homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
})
export class HomepageComponent implements OnInit {
  
  submitted;
  
  constructor(
    private router: Router,
    private datastore: DatastoreService, 
    private intercomm: IntercommService,
    private translate: TranslateService
  ) {
    
    if(this.datastore.getValue("language") == null) this.datastore.setValue("language", "en"); 
    translate.use(this.datastore.getValue("language"));
    this.intercomm.selectedLanguage.next(this.datastore.getValue("language"));
    
    // Subscribe to the selectedLanguage Subject of the intercomm service
    this.intercomm.selectedLanguage.asObservable().subscribe((value: any) => {
        translate.use(value);
    });
      
  }
  
  ngOnInit() {
     
  }
  
  go() {
    this.router.navigate(['/artist/event']);
  }
  
  onSubmit() { 
    event.preventDefault();
    this.submitted = true;
  }

}
