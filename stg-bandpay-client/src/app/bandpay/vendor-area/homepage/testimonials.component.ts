import { Component, OnInit } from '@angular/core';

@Component({ 
  selector: 'testimonials',
  styles: [],
  templateUrl: './testimonials.component.html',
})

export class UITestimonialsComponent implements OnInit {
  testimonials;

  constructor() {}

  getTestimonials(): void {
    this.testimonials = [
      {
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque ratione consequuntur ut placeat.',
        avatar: 'assets/images-demo/avatars/1.jpg',
        name: 'Jason Bourne',
        title: 'Senior PM'
      }, {
        content: 'Cum suscipit voluptatem modi repellat consequuntur aliquid nostrum, dolore pariatur consequatur nobis',
        avatar: 'assets/images-demo/avatars/2.jpg',
        name: 'Bella Swan',
        title: 'VP Product'
      }, {
        content: 'Temporibus nesciunt quod magnam dicta ea, quae minima tempore eiciendis nisi ab, perferendis',
        avatar: 'assets/images-demo/avatars/3.jpg',
        name: 'Min Chan',
        title: 'Engineer Lead'
      }
    ];
  }

  ngOnInit(): void {
    this.getTestimonials();
  }
  
  
}
