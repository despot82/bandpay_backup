import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { VendorProfile, BankData, PaypalData } from '../../shared/vendor_profile';
import { RestService } from '../../shared/rest.service';
import { DatastoreService } from '../../shared/datastore.service';
import { Slot } from '../../shared/slot';
import { SlotModel } from '../../shared/SlotModel';
import { Buyer } from '../../shared/buyer';
import { parseDatetime } from '../../shared/parsing-helper';
import { Headers, ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'slots-view',
  templateUrl: './slots-view.component.html',
  styleUrls: ['./slots-view.component.css']
})
export class SlotsViewComponent implements OnInit {

    slots: SlotModel[];

    constructor(private rest: RestService, private datastore: DatastoreService, private router: Router) {

    }

    ngOnInit() {
      this.rest.get(`/backoffice/slot`).subscribe((response) => {
        //alert(response.json());

        this.slots = this.formIntegrationsArrayFromStream(response);

      });

    }

    formIntegrationsArrayFromStream(response) {

      let newSlotsModels = [];

      response.json().forEach(function(item){

          let slot = new SlotModel({
              id:item.id,
              vendor:item.vendor,
              buyer:item.buyer, 
          });

          newSlotsModels.push(slot);
      });

      return newSlotsModels;
    }
}
