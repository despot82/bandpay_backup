import { RouterModule, Routes } from '@angular/router';
import { SlotsViewComponent } from './slots-view/slots-view.component';

const AdminAreaRoutes: Routes = [
  { path: 'admin', redirectTo: '/admin/slots', pathMatch: 'full' },
  { path: 'admin/slots', component: SlotsViewComponent, pathMatch: 'full' }

];

export const AdminAreaRoutingModule = RouterModule.forChild(AdminAreaRoutes);
