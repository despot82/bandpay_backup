import { NgModule } from '@angular/core';
import { AdminAreaComponent } from './admin-area.component';
import { AdminAreaRoutingModule } from './admin-area-routing.module';

import { SlotsViewComponent } from './slots-view/slots-view.component';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { Ng2Webstorage } from 'ngx-webstorage';
import { AgmCoreModule } from '@agm/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RestService } from '../shared/rest.service';
import { JwtHttp } from '../shared/jwt-http.service';
import { LoginService } from '../shared/login.service';
import { DatastoreService } from '../shared/datastore.service';
import { GooglePlacesAutocompleteDirective } from '../shared/google-places-autocomplete.directive';
import { SharedModule } from '../shared/shared.module';
import { CommonModule, JsonPipe } from '@angular/common';
import { MaterialModule, DateAdapter } from '@angular/material';
import { MomentDateAdapter, MOMENT_DATE_FORMATS } from '../shared/moment-date-adapter';
import { MD_DATE_FORMATS, MD_NATIVE_DATE_FORMATS } from '@angular/material';


@NgModule({
  imports: [
    AdminAreaRoutingModule,
    SharedModule,

    FormsModule,
    CommonModule,
    MaterialModule,

    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA-MvpqfnVZqzuOMU6-oS_wF-4Kn_SHDSU',
      libraries: ['places']
    }),
    Ng2Webstorage,
    AngularFontAwesomeModule,
  ],
  declarations: [
    AdminAreaComponent,
    SlotsViewComponent
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter },
    { provide: MD_DATE_FORMATS, useValue: MOMENT_DATE_FORMATS },
  ]
})
export class AdminAreaModule { }
