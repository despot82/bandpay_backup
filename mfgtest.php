<?php
$first_call="<cardNumberRequest protocol='getVirtualCardNumber' version='2.9'>
  <externalReference>order-1519127574</externalReference>
  <gender>male</gender>
  <firstName>Fritz</firstName>
  <lastName>Bianchi</lastName>
  <street>Roten str</street>
  <city>Sant Gallen</city>
  <zip>9000</zip>
  <country>CH</country>
  <language>it</language>
  <email>test@mfgroup.ch</email>
  <birthdate>1975-03-06</birthdate>
  <merchantId>2345</merchantId>
  <filialId>33231</filialId>
  <terminalId>4436</terminalId>
  <amount>1500</amount>
  <currencyCode>CHF</currencyCode>
</cardNumberRequest>";

$second_call="<FinancialRequest version='1.0' protocol='PaymentServer_V2_9' msgnum='1519130084'>
<CardNumber>6004516000000000300</CardNumber>
<RequestDate>20180220133444</RequestDate>
<TransactionType>debit</TransactionType>
<Currency>CHF</Currency>
<Amount>5000</Amount>
<ExternalReference>your reference</ExternalReference>
<MerchantId>2345</MerchantId>
<FilialId>3323</FilialId>
<TerminalId>4436</TerminalId>
</FinancialRequest>";

define('PSG_SERVER','https://testgateway.mfgroup.ch');

function request($xml, $user, $psw) {
 $credentials="$user:$psw";
 $ch= curl_init();
 curl_setopt($ch, CURLOPT_URL, PSG_SERVER);
 curl_setopt($ch, CURLOPT_TIMEOUT, 60);
 curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $psw);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 curl_setopt($ch, CURLOPT_POST, true);
 curl_setopt($ch, CURLAUTH_BASIC, true);
 curl_setopt($ch, CURLOPT_POSTFIELDS, 'xml=' . urlencode($xml));
 $data= curl_exec($ch);
 if (curl_errno($ch)) {
 echo "Error: ". curl_error($ch);
 return;
 }
 $result= simplexml_load_string($data);
 curl_close($ch);
 return $result;
}

echo "<pre>"; print_r(request($second_call,'stagedTest','kBdGI86m')); echo "</pre>";

?>