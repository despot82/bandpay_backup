<?php

use Bandpay\Database\DB;
use Bandpay\Database\MySQLQueryBuilder as QB;

define('STG_PROFILE_IMAGES_URL', 'http://t1.stagend.ch/uploads/profiles');

function stg_sync_user(int $stg_id) {
	$query = (new QB(DB::get()))
			->insert('bp_user', ['stg_id', 'first_name', 'last_name', 'email'])
			->select('stg_user', ['stg_id', 'first_name', 'last_name', 'email'])
			->where(['stg_id' => $stg_id])
			->onDuplicateKeyUpdate([
		'first_name' => 'VALUES(first_name)',
		'last_name' => 'VALUES(last_name)',
		'email' => 'VALUES(email)'
	]);
	$query->execute();
}

function stg_sync_vendors(int $stg_id) {
	$query = (new QB(DB::get()))
			->insert('bp_profile', ['user_id', 'stg_profile_id', 'name', 'email', 'phone_code', 'phone'])
			->select('stg_vendor AS v', ['u.id AS user_id', 'v.stg_profile_id', 'v.profile_name AS name', 'v.email', 'v.phone_code', 'v.phone'])
			->join(['bp_user AS u'], ['u.stg_id' => 'v.stg_id'])
			->where(['v.stg_id' => $stg_id])
			->onDuplicateKeyUpdate([
		'email' => 'VALUES(email)',
		'phone_code' => 'VALUES(phone_code)',
		'phone' => 'VALUES(phone)'
	]);
	$query->execute();
}

function stg_sync_bank(int $stg_id) {
	// Join tables bp_user and bp_profile to get the internal profile_id associated with the stagend profile.
	$paypal_query = (new QB(DB::get()))
			->insert('bp_paypal_data', ['profile_id', 'email'])
			->select('stg_banking AS b', ['p.id AS profile_id', 'b.paypal_email as email'])
			->join(['bp_user AS u', 'bp_profile AS p'], [
				'u.stg_id' => 'b.stg_id',
				'p.user_id' => 'u.id',
				'p.stg_profile_id' => 'b.stg_profile_id'
			])
			->where([
				'b.stg_id' => $stg_id,
				'type' => 'paypal'
			])
			->onDuplicateKeyUpdate(['email' => 'VALUES(email)']);
	$paypal_query->execute();
	$bank_query = (new QB(DB::get()))
			->insert('bp_bank_data', ['profile_id', 'beneficiary', 'street', 'zip', 'city', 'country', 'iban'])
			->select('stg_banking AS b', ['p.id AS profile_id', 'b.beneficiary', 'b.street', 'b.zip', 'b.city', 'b.country', 'b.iban'])
			->join(['bp_user AS u', 'bp_profile AS p'], [
				'u.stg_id' => 'b.stg_id',
				'p.user_id' => 'u.id',
				'p.stg_profile_id' => 'b.stg_profile_id'
			])
			->where([
				'b.stg_id' => $stg_id,
				'type' => 'bank'
			])
			->onDuplicateKeyUpdate([
		'beneficiary' => 'VALUES(beneficiary)',
		'street' => 'VALUES(street)',
		'zip' => 'VALUES(zip)',
		'city' => 'VALUES(city)',
		'country' => 'VALUES(country)',
		'iban' => 'VALUES(iban)'
	]);
	$bank_query->execute();
}

function stg_sync_user_image(int $stg_id) {
	$query = (new QB(DB::get()))
			->insert('stg_user_image', ['user_id', 'image_url'])
			->select('stg_user', ['bp_user.id', 'stg_user.image_url'])
			->join(['bp_user'], ['bp_user.stg_id' => 'stg_user.stg_id'])
			->where([
				'bp_user.stg_id' => $stg_id,
				'stg_user.image_url IS NOT NULL'
			])
			->onDuplicateKeyUpdate([
		'image_url' => 'VALUES(image_url)'
	]);
	$query->execute();
}

function stg_sync_all(int $stg_id) {
	stg_sync_user($stg_id);
	stg_sync_vendors($stg_id);
	stg_sync_bank($stg_id);
	stg_sync_user_image($stg_id);
}

function stg_verify_user(string $email, string $password) {
	$data = (new QB(DB::get()))
			->select('stg_user', ['stg_id', 'salt', 'hash'])
			->where(['email' => $email])
			->get();
	$salt = $data['salt'];
	$hash = $data['hash'];
	$stg_id = $data['stg_id'];

	return sha1($salt . $password) === $hash ? $stg_id : FALSE;
}
