<?php

use Bandpay\Database\MySQLQueryBuilder as QB;
use Bandpay\Database\DB;
use Bandpay\Mailing\BandpayMailer as BPMailer;

define('CHECKOUT_URL', CLIENT_URL . '/checkout');

//Send the payment invitation email to the buyer, with the link for payment page
function send_checkout_mail(string $token) {
	
	$token_data = (new QB(DB::get()))
	    ->select('bp_checkout_token')
		->where(['token' => $token])
		->get();

	$slot_id = $token_data['slot_id'];

	$query = (new QB(DB::get()))
	    ->select('bp_slot s', ['CONCAT_WS(" ",b.first_name,b.last_name) buyer_name', 'b.email buyer_email', 'v.name vendor_name'])
		->join(['bp_buyer b', 'bp_profile v'], ['b.id' => 's.buyer_id', 'v.id' => 's.vendor_id'])
		->where(['s.id' => $slot_id]);

	$params = $query->get();
    $params['checkout_url'] = CHECKOUT_URL . "/$token";

	$mail = new BPMailer(TRUE);
    $mail -> Subject = 'BandPay checkout';
	$mail -> addAddress($params['buyer_email'], $params['buyer_name']);
	$mail -> bodyFromHtml(dirname(dirname(__FILE__)) . '/mail_templates/checkout.html', $params);
	
	error_log(print_r($params, true), 3, "/var/www/apibandpay/v1/php_errors.log");
	
	$mail -> send();

}

// Send the payment confirmation email to the buyer
function send_confirmation_email_to_buyer(int $slot_id) {
	$query = (new QB(DB::get()))
	    ->select('bp_slot s', ['CONCAT_WS(" ",b.first_name,b.last_name) buyer_name', 'b.email as buyer_email', 'v.name vendor_name', 'v.email vendor_email', 's.category slot_type', 's.start_time slot_date'])
	    ->join(['bp_buyer b', 'bp_profile v'], ['b.id' => 's.buyer_id', 'v.id' => 's.vendor_id'])
	    ->where(['s.id' => $slot_id]);
	
	$params = $query->get();
	$params['slot_date'] = (new DateTime($params['slot_date']))
	    ->format('Y-m-d');
	
	$mailer = new BPMailer(TRUE);
	$mailer -> Subject = 'BandPay checkout confirmation';
	$mailer -> addAddress($params['buyer_email'], $params['buyer_name']);
	$mailer -> bodyFromHtml(API_ROOT . '/mail_templates/checkout_buyer_confirmation.html', $params);
	
	error_log(print_r($params, true), 3, "/var/www/apibandpay/v1/php_errors.log");
	
	$mailer -> send();
}

// Send the payment confirmation email to vendor (to the artist)
function send_confirmation_email_to_vendor(int $slot_id) {
	$query = (new QB(DB::get()))
	    ->select('bp_slot s', ['CONCAT_WS(" ",b.first_name,b.last_name) buyer_name', 'b.email as buyer_email', 'v.name vendor_name', 'v.email vendor_email', 's.category slot_type', 's.start_time slot_date'])
	    ->join(['bp_buyer b', 'bp_profile v'], ['b.id' => 's.buyer_id', 'v.id' => 's.vendor_id'])
	    ->where(['s.id' => $slot_id]);
	
	$params = $query->get();
	$params['slot_date'] = (new DateTime($params['slot_date']))
	    ->format('Y-m-d');

	$mailer = new BPMailer(TRUE);
	$mailer -> Subject = 'BandPay checkout confirmation';
	$mailer -> addAddress($params['vendor_email'], $params['vendor_name']);
	$mailer -> bodyFromHtml(API_ROOT . '/mail_templates/checkout_vendor_confirmation.html', $params);
	
	error_log(print_r($params, true), 3, "/var/www/apibandpay/v1/php_errors.log");
	
	$mailer -> send();
}

// Send the confirmation email to stagend
function send_confirmation_email_to_stagend(int $slot_id) {
	$query = (new QB(DB::get()))
	    ->select('bp_slot s', ['CONCAT_WS(" ",b.first_name,b.last_name) buyer_name', 'b.email as buyer_email', 'v.name vendor_name', 'v.email vendor_email', 's.category slot_type', 's.start_time slot_date'])
	    ->join(['bp_buyer b', 'bp_profile v'], ['b.id' => 's.buyer_id', 'v.id' => 's.vendor_id'])
	    ->where(['s.id' => $slot_id]);
	
	$params = $query->get();
	$params['slot_date'] = (new DateTime($params['slot_date']))
	    ->format('Y-m-d');
	
	$mailer = new BPMailer(TRUE);
	$mailer -> Subject = 'BandPay checkout flow completed';
	$mailer -> addAddress("info@stagend.com", "Stagend - Checkout confirmation");
	$mailer -> bodyFromHtml(API_ROOT . '/mail_templates/checkout_stagend_confirmation.html', $params);
	
	error_log(print_r($params, true), 3, "/var/www/apibandpay/v1/php_errors.log");
	
	$mailer -> send();
}

