<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_class_short_name(string $class_name) {
	$refl_class = new ReflectionClass($class_name);
	return $refl_class->getShortName();
}