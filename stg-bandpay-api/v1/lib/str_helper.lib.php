<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function str_to_snake($str) {
	return strtolower(preg_replace('/([[:lower:]])([[:upper:]])/', '$1_$2', $str));
}

function str_to_camel($str, $ucfirst= FALSE) {
	$str = str_replace('_', '', ucwords($str, '_'));
	if (!$ucfirst) {
		$str = lcfirst($str);
	}
	return $str;
}