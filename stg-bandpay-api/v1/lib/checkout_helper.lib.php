<?php

use Bandpay\Database\MySQLQueryBuilder as QB;
use Bandpay\Database\DB;

define('CHECKOUT_TOKEN_EXP_AFTER', '+2 months');

function issue_checkout_token(int $slot_id): string {
	$token = uniqid('', TRUE);
	$expire_date = new DateTime(CHECKOUT_TOKEN_EXP_AFTER);
	
	(new QB(DB::get()))
			->insert('bp_checkout_token', [
				'slot_id' => $slot_id,
				'token' => $token,
				'expires' => $expire_date->format('Y-m-d H:i:s')
			])
			->execute();
	return $token;
}

function get_token_data(string $token): array {
	return (new QB(DB::get()))
				->select('bp_checkout_token')
				->where(['token' => $token])
				->get();
}