<?php

function array_is_assoc(array $array) {
	foreach (array_keys($array) as $key) {
		if (is_string($key)) {
			return TRUE;
		}
		return FALSE;
	}
}