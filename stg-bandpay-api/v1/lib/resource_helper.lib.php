<?php

define('QUERY_SPECIAL_PARAMS', ['fields', 'embed']);

function parse_query_params(array $query_params) {
	$parsed = [];
	$parsing_search = FALSE;
	array_walk($query_params, function ($value, $key) use (&$parsed, &$parsing_search) {
		if ($key === 'search') {
			$parsing_search = TRUE;
		} else if (in_array($key, QUERY_SPECIAL_PARAMS)) {
			$parsed[$key] = explode(',', $value);
		} else if ($parsing_search) {
			$parsed['search'][$key] = $value;
		}
	});
	return $parsed;
}

function create_where_condition(array $columns) {
	return implode(' AND ', array_map(function ($v) {
				return $v . ' = ?';
			}, $columns)
	);
}
