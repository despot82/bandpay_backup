<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// define('ROOT', $_SERVER['CONTEXT_DOCUMENT_ROOT']);
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('API_VERSION', 'v1');
define('API_ROOT', ROOT .'/'. API_VERSION);
define('LOG_DIR', ROOT . '/logs');
define('ROOT_URL', (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] === '80' ? '' : $_SERVER['SERVER_PORT']) . $_SERVER['CONTEXT_PREFIX']);
define('API_URL', ROOT_URL . '/' . API_VERSION);
define('API_ALLOW_ORIGIN', ['https://www.gigpay.ch', 'http://www.gigpay.ch', 'http://localhost:4200', 'https://gigpay.ch', 'http://gigpay.ch','https://gigpay.de', 'http://s.stagend.ch']);
define('CLIENT_URL', 'https://gigpay.ch');

define('FEE_PERCENT', 3.9);
define('PAYMENT_DEADLINE', '+2 weeks');

require(ROOT . '/vendor/autoload.php');

function get_api_users() {
	$db = Bandpay\Database\DB::get();
	$stm = $db->query('SELECT * FROM bp_api_user;');
	return $stm->fetchAll(PDO::FETCH_KEY_PAIR);
}

// Display errors in browser.
error_reporting(E_ALL);
ini_set('display_errors', '1');

// Register class autoload function simply load from "classes" folder and subfolders.
spl_autoload_register(function ($class_name) {
	$name_arr = explode('\\', $class_name);
	$short_name = end($name_arr);

	$iterator = new RegexIterator(
			new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator(API_ROOT . '/classes/')
			), '/^.+\.class\.php$/i', RegexIterator::GET_MATCH
	);

	foreach ($iterator as $path) {
		// RegexIterator always returns an array.
		$path = $path[0];
		if (basename($path, '.class.php') === $short_name) {
			require_once($path);
			return;
		}
	}
});

$api_users = get_api_users();
