<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once './init.php';

include './lib/str_helper.lib.php';

$test_camel = [
	'wakeUp',
	'grabTheBrushAndPutALittleMakeUp',
	'hideTheScarsToFadeAwayTheShakeUp',
	'test'
];

$test_snake = [
	'i_dont_think_you_trust_in_my',
	'self_righteous_suicide',
	'i_cry_when_angels_deserve_to_die'
];

header('Content-Type: text/plain');

print "camelCase => snake_case\n";
foreach ($test_camel as $str) {
	print $str . " => " . str_to_snake($str) . "\n";
}

print "\nsnake_case => camelCase\n";
foreach ($test_snake as $str) {
	print $str . " => " . str_to_camel($str) . "\n";
}

print "\nsnake_case => CamelCase (ucfirst)\n";
foreach ($test_snake as $str) {
	print $str . " => " . str_to_camel($str, TRUE) . "\n";
}
