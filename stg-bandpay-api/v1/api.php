<?php

/*
 * Project TODOs
 * 
 */

require_once 'init.php';

include API_ROOT . '/lib/stg_helper.lib.php';
include API_ROOT . '/lib/resource_helper.lib.php';
include API_ROOT . '/lib/mail_helper.lib.php';
include API_ROOT . '/lib/checkout_helper.lib.php';

use Slim\Http\Request;
use Slim\Http\Response;
use Bandpay\Database\MySQLQueryBuilder as QB;
use \Knp\Snappy\Pdf;

$app = new Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false
    ]
]);

$container = $app->getContainer();

// Application dependencies here.
// Logger dependency.
$container['logger'] = function () {
    $logger = new \Monolog\Logger('bandpay_api_logger');
    $file_handler = new \Monolog\Handler\StreamHandler(API_ROOT . '/../logs/api.log');
    $logger->pushHandler($file_handler);
    $logger->pushProcessor(function ($record) {
        $record['extra']['api_version'] = API_VERSION;
        return $record;
    });
    return $logger;
};

// DBs dependencies.
$container['db'] = \Bandpay\Database\DB::get();

// Handlers.
$container['notFoundHandler'] = function ($c) {
    return new Slim\Handlers\NotFound();
};

$container['notAllowedHandler'] = function ($c) {
    return new Slim\Handlers\NotAllowed();
};

$container['unauthorizedHandler'] = function ($c) {
    return new Slim\Handlers\Unauthorized();
};

// Middlewares here.
//$app->add(function (Request $request, Response $response, callable $next) {
//  $request = $request
//          ->withQueryParams(array_merge($_GET, ['XDEBUG_SESSION_START' => 'netbeans-xdebug']))
//          ->withCookieParams(array_merge($_COOKIE, ['XDEBUG_SESSION' => 'netbeans-xdebug']));
//
//  return $next($request, $response);
//});
// Monitor execution time.
// OBSOLETE: Replaced by ServerTiming middleware.
//$app->add(new \Bandpay\Middleware\ExecutionTime([
//  'logger' => $container['logger']
//]));
// Redirect URLs that end with trailing slash to non-trailing slash ones.
$app->add(new \Psr7Middlewares\Middleware\TrailingSlash(false));

// Allow cross origin from Node.js.
$app->add(new Tuupola\Middleware\Cors([
    'origin' => API_ALLOW_ORIGIN,
    'methods' => ['OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'],
    'headers.allow' => ['X-Requested-With', 'Content-Type', 'Accept', 'Origin', 'Authorization', 'Location'],
    'headers.expose' => ['Location']
]));

// JSON parser.
$app->add(function (Request $request, Response $response, callable $next) {
    $request->registerMediaTypeParser(
        MediaType::JSON,
        function ($data) {
            $parsed = json_decode($data, true);
            if (is_null($parsed)) {
                throw new InvalidArgumentException('Malformed JSON body.');
            }
            return $parsed;
        }
    );

    return $next($request, $response);
});

/*
 * Authentication scheme:
 * "/public": No authentication at all.
 * "/token": Basic HTTP authentication with credentials from DB (bp_api_user).
 * Rest of the API: JWT authentication signed with secret from DB (bp_jwt_secret).
 */

$app->add(new Tuupola\Middleware\HttpBasicAuthentication([
    'path' => ['/token'],
    'ignore' => ['/public'],
    'secure' => false,
    'error' => function (Request $request, Response $response) use ($container) {
        return ($container->unauthorizedHandler)($request, $response, 'Basic');
    },
    'users' => $api_users,
]));

$app->add(new Tuupola\Middleware\JwtAuthentication([
    'path' => ['/'],
    'ignore' => ['/token', '/public'],
    'secure' => false,
    'logger' => $container['logger'],
    'error' => function (Request $request, Response $response) use ($container) {
        // TODO: Add message for expired token.
        return ($container->unauthorizedHandler)($request, $response, 'Bearer');
    },
    'secret' => Bandpay\JWT\Token::getSecret()
]));

$app->add(new \Tuupola\Middleware\ServerTiming());

// REST endopoints here.
// Public
$app->group('/public', function () {
    $this->get('/test', function (Request $request, Response $response) {
        return $response->withJson(['data' => 'This is from the API']);
    });

    $this->get('/teapot', function (Request $request, Response $response) {
        return $response->withStatus(418);
    });
});

// Issue a new token.
$app->post('/token', function (Request $request, Response $response) {
    $secret = $request->getParsedBodyParam('secret');
    if (!$secret) {
        return ($this->unauthorizedHandler)($request, $response, 'Basic');
    }

    if (false != $token = Bandpay\JWT\Token::issue($secret)) {
        // Wrap token for security reasons.
        return $response->withJson(['token' => $token]);
    }

    return ($this->unauthorizedHandler)($request, $response, 'Basic');
});

$app->group('/stagend', function () {
    $this->group('/vendor', function () {
        $this->post('', function (Request $request, Response $response) {
            $email = $request->getParsedBodyParam('email');
            $password = $request->getParsedBodyParam('password');
            $stg_id = stg_verify_user($email, $password);

            if ($stg_id === false) {
                return ($this->unauthorizedHandler)($request, $response, 'Basic');
            }

            // TODO: Test image sync.
            stg_sync_all($stg_id);

            $id = (new QB($this->db))
                ->select('bp_user', ['id'])
                ->where(['stg_id' => $stg_id])
                ->getColumn();

            // Issue a new JWT including user's credetials.
            $token = Bandpay\JWT\Token::issue(Bandpay\JWT\Token::getSecret(), ['user_id' => $id]);

            return $response
                ->withStatus(201)
                ->withHeader('Location', API_URL . '/vendor/' . $id)
                ->withJson(['token' => $token]);
        });
    });
});

$app->group('/vendor', function () {
    $this->post('', function (Request $request, Response $response) {
        $body = $request->getParsedBody();
        $query = (new QB($this->db))
            ->insert('bp_user', $body);
        $query->execute();

        $id = $this->db->lastInsertId();

        // Issue a new JWT including user's credetials.
        $token = Bandpay\JWT\Token::issue(Bandpay\JWT\Token::getSecret(), ['user_id' => $id, 'anonymous' => true]);

        return $response
            ->withStatus(201)
            ->withHeader('Location', API_URL . '/vendor/' . $id)
            ->withJson(['token' => $token]);
    });

    $this->get('', function (Request $request, Response $response) {
        $parsed_params = parse_query_params($request->getQueryParams());
        $search = $parsed_params['search'] ?? null;
        $fields = $parsed_params['fields'] ?? null;
        $embed = $parsed_params['embed'] ?? [];
        $query = (new QB($this->db))
            ->select('bp_user', $fields);
        if ($search) {
            $query = $query->where($search);
        }
        $vendors = $query->getAll();
        if (in_array('profile', $embed)) {
            foreach ($vendors as &$vendor) {
                $vendor['profiles'] = (new QB($this->db))
                    ->select('bp_profile')
                    ->where(['user_id' => $vendor['id']])
                    ->getAll();
            }
        }
        return $response->withJson($vendors);
    });

    $this->get('/{id}', function (Request $request, Response $response) {
        $id = $request->getAttribute('id');
        $parsed_params = parse_query_params($request->getQueryParams());
        $fields = $parsed_params['fields'] ?? null;
        $embed = $parsed_params['embed'] ?? [];
        $vendor = (new QB($this->db))
            ->select('bp_user', $fields)
            ->where(['id' => $id])
            ->get();
        if (in_array('profile', $embed)) {
            $vendor['profiles'] = (new QB($this->db))
                ->select('bp_profile')
                ->where(['user_id' => $id])
                ->getAll();
        }
        return $response->withJson($vendor);
    });

    $this->get('/{id}/image', function (Request $request, Response $response) {
        $user_id = $request->getAttribute('id');
        $image_url = (new QB($this->db))
            ->select('stg_user_image', ['image_url'])
            ->where(['user_id' => $user_id])
            ->getColumn();
        $image = null;
        if ($image_url) {
            $image = file_get_contents(STG_PROFILE_IMAGES_URL . $image_url);
            // TODO: That's the only way to get MIME type for an image. Problem with finfo/mime_content_type on Windows?
            $mime = image_type_to_mime_type(exif_imagetype(STG_PROFILE_IMAGES_URL . $image_url));
            $response->write($image);
            return $response->withHeader('Content-Type', $mime);
        }
        else {
            return ($this->notFoundHandler)($request, $response);
        }
    });

    $this->post('/{id}/profile', function (Request $request, Response $response) {
        $user_id = $request->getAttribute('id');
        $body = $request->getParsedBody();
        $body['user_id'] = $user_id;
        unset($body['paypal_data']);
        unset($body['bank_data']);

        $paypal_data = $request->getParsedBodyParam('paypal_data');
        $bank_data = $request->getParsedBodyParam('bank_data');

        $query = (new QB($this->db))
            ->insert('bp_profile', $body);
        $query->execute();
        $profile_id = $this->db->lastInsertId();

        if ($paypal_data) {
            $paypal_data['profile_id'] = $profile_id; (new QB($this->db))
                ->insert('bp_paypal_data', $paypal_data)
                ->onDuplicateKeyUpdate(['email' => 'VALUES(email)'])
                ->execute(); (new QB($this->db))
                ->delete('bp_bank_data')
                ->where(['profile_id' => $profile_id])
                ->execute();
        }
        elseif ($bank_data) {
            $bank_data['profile_id'] = $profile_id; (new QB($this->db))
                ->insert('bp_bank_data', $bank_data)
                ->onDuplicateKeyUpdate([
                'beneficiary' => 'VALUES(beneficiary)',
                'street' => 'VALUES(street)',
                'zip' => 'VALUES(zip)',
                'city' => 'VALUES(city)',
                'iban' => 'VALUES(iban)'
            ])
                ->execute(); (new QB($this->db))
                ->delete('bp_paypal_data')
                ->where(['profile_id' => $profile_id])
                ->execute();
        }
        return $response
            ->withStatus(201)
            ->withHeader('Location', API_URL . '/profile/' . $profile_id)
            ->withJson(['id' => $profile_id]);
    });

    $this->get('/{id}/profile', function (Request $request, Response $response) {
        $user_id = $request->getAttribute('id');
        $parsed_params = parse_query_params($request->getQueryParams());
        $search = $parsed_params['search'] ?? [];
        $fields = $parsed_params['fields'] ?? null;
        $embed = $parsed_params['embed'] ?? [];
        $profiles = (new QB($this->db))
            ->select('bp_profile', $fields)
            ->where(array_merge(['user_id' => $user_id], $search))
            ->getAll();
        if (in_array('banking', $embed)) {
            foreach ($profiles as &$profile) {
                $paypal_data = (new QB($this->db))
                    ->select('bp_paypal_data', ['email'])
                    ->where(['profile_id' => $profile['id']])
                    ->get();
                $bank_data = (new QB($this->db))
                    ->select('bp_bank_data', ['beneficiary', 'street', 'zip', 'city', 'iban'])
                    ->where(['profile_id' => $profile['id']])
                    ->get();
                if ($paypal_data != null) {
                    $profile['paypal_data'] = $paypal_data;
                }
                elseif ($bank_data != null) {
                    $profile['bank_data'] = $bank_data;
                }
            }
        }
        return $response->withJson($profiles);
    });
});

$app->group('/buyer', function () {
    $this->post('', function (Request $request, Response $response) {
        $body = $request->getParsedBody();
        $query = (new QB($this->db))
            ->insert('bp_buyer', $body);
        $query->execute();

        $id = $this->db->lastInsertId();

        return $response
            ->withStatus(201)
            ->withHeader('Location', API_URL . '/buyer/' . $id)
            ->withJson(['id' => $id]);
    });

    $this->get('', function (Request $request, Response $response) {
        $parsed_params = parse_query_params($request->getQueryParams());
        $search = $parsed_params['search'] ?? null;
        $fields = $parsed_params['fields'] ?? null;
        $query = (new QB($this->db))
            ->select('bp_buyer', $fields);
        if ($search) {
            $query = $query->where($search);
        }
        $buyers = $query->getAll();

        return $response->withJson($buyers);
    });

    $this->get('/{id}', function (Request $request, Response $response) {
        $id = $request->getAttribute('id');
        $parsed_params = parse_query_params($request->getQueryParams());
        $fields = $parsed_params['fields'] ?? null;
        $buyer = (new QB($this->db))
            ->select('bp_buyer', $fields)
            ->where(['id' => $id])
            ->get();

        return $response->withJson($buyer);
    });
    
    // When a buyer goes to /checkout/TOKEN link, he will use this PATCH
    // to change his fn, ln, city, address, zip, country
    $this->patch('/{id}', function (Request $request, Response $response) {
    	$buyer_id = $request->getAttribute('id');
    
    	$body = $request->getParsedBody(); 
    	(new QB($this->db))
    	->update('bp_buyer', $body)
    	->where(['id' => $buyer_id])
    	->execute();
    	
    	return $response->withStatus(204);
    });
});

$app->group('/profile', function () {
    $this->get('', function (Request $request, Response $response) {
        $parsed_params = parse_query_params($request->getQueryParams());
        $search = $parsed_params['search'] ?? [];
        $fields = $parsed_params['fields'] ?? null;
        $embed = $parsed_params['embed'] ?? [];
        $query = (new QB($this->db))
            ->select('bp_profile', $fields);
        if ($search) {
            $query = $query->where($search);
        }
        $profiles = $query->getAll();
        if (in_array('banking', $embed)) {
            foreach ($profiles as &$profile) {
                $paypal_data = (new QB($this->db))
                    ->select('bp_paypal_data', ['email'])
                    ->where(['profile_id' => $profile['id']])
                    ->get();
                $bank_data = (new QB($this->db))
                    ->select('bp_bank_data', ['beneficiary', 'street', 'zip', 'city', 'iban'])
                    ->where(['profile_id' => $profile['id']])
                    ->get();
                if ($paypal_data != null) {
                    $profile['paypal_data'] = $paypal_data;
                }
                elseif ($bank_data != null) {
                    $profile['bank_data'] = $bank_data;
                }
            }
        }
        return $response->withJson($profiles);
    });

    $this->get('/{id}', function (Request $request, Response $response) {
        $profile_id = $request->getAttribute('id');
        $parsed_params = parse_query_params($request->getQueryParams());
        $fields = $parsed_params['fields'] ?? null;
        $embed = $parsed_params['embed'] ?? [];
        $profile = (new QB($this->db))
            ->select('bp_profile', $fields)
            ->where(['id' => $profile_id])
            ->get();
        if (in_array('banking', $embed)) {
            $paypal_data = (new QB($this->db))
                ->select('bp_paypal_data', ['email'])
                ->where(['profile_id' => $profile['id']])
                ->get();
            $bank_data = (new QB($this->db))
                ->select('bp_bank_data', ['beneficiary', 'street', 'zip', 'city', 'iban'])
                ->where(['profile_id' => $profile['id']])
                ->get();
            if ($paypal_data != null) {
                $profile['paypal_data'] = $paypal_data;
            }
            elseif ($bank_data != null) {
                $profile['bank_data'] = $bank_data;
            }
        }
        return $response->withJson($profile);
    });

    $this->put('/{id}', function (Request $request, Response $response) {
        $profile_id = $request->getAttribute('id');
        // Manually fill body with all values to also upadate null ones (see PUT method specification).
        $body = [
            'user_id' => $request->getParsedBodyParam('user_id'),
            'name' => $request->getParsedBodyParam('name'),
            'email' => $request->getParsedBodyParam('email'),
            'phone_code' => $request->getParsedBodyParam('phone_code'),
            'phone' => $request->getParsedBodyParam('phone')
        ]; (new QB($this->db))
            ->update('bp_profile', $body)
            ->where(['id' => $profile_id])
            ->execute();

        $paypal_data = $request->getParsedBodyParam('paypal_data');
        $bank_data = $request->getParsedBodyParam('bank_data');
        if ($paypal_data) {
            $paypal_data['profile_id'] = $profile_id; (new QB($this->db))
                ->insert('bp_paypal_data', $paypal_data)
                ->onDuplicateKeyUpdate(['email' => 'VALUES(email)'])
                ->execute(); (new QB($this->db))
                ->delete('bp_bank_data')
                ->where(['profile_id' => $profile_id])
                ->execute();
        }
        elseif ($bank_data) {
            $bank_data['profile_id'] = $profile_id; (new QB($this->db))
                ->insert('bp_bank_data', $bank_data)
                ->onDuplicateKeyUpdate([
                'beneficiary' => 'VALUES(beneficiary)',
                'street' => 'VALUES(street)',
                'zip' => 'VALUES(zip)',
                'city' => 'VALUES(city)',
                'iban' => 'VALUES(iban)'
            ])
                ->execute(); (new QB($this->db))
                ->delete('bp_paypal_data')
                ->where(['profile_id' => $profile_id])
                ->execute();
        }

        return $response->withStatus(204);
    });

    $this->patch('/{id}', function (Request $request, Response $response) {
        $profile_id = $request->getAttribute('id');
        // Manually fill body with all values to also upadate null ones (see PUT method specification).
        $body = $request->getParsedBody();
        unset($body['paypal_data']);
        unset($body['bank_data']); (new QB($this->db))
            ->update('bp_profile', $body)
            ->where(['id' => $profile_id])
            ->execute();

        $paypal_data = $request->getParsedBodyParam('paypal_data');
        $bank_data = $request->getParsedBodyParam('bank_data');
        if ($paypal_data) {
            $paypal_data['profile_id'] = $profile_id; (new QB($this->db))
                ->insert('bp_paypal_data', $paypal_data)
                ->onDuplicateKeyUpdate(['email' => 'VALUES(email)'])
                ->execute(); (new QB($this->db))
                ->delete('bp_bank_data')
                ->where(['profile_id' => $profile_id])
                ->execute();
        }
        elseif ($bank_data) {
            $bank_data['profile_id'] = $profile_id; (new QB($this->db))
                ->insert('bp_bank_data', $bank_data)
                ->onDuplicateKeyUpdate([
                'beneficiary' => 'VALUES(beneficiary)',
                'street' => 'VALUES(street)',
                'zip' => 'VALUES(zip)',
                'city' => 'VALUES(city)',
                'iban' => 'VALUES(iban)'
            ])
                ->execute(); (new QB($this->db))
                ->delete('bp_paypal_data')
                ->where(['profile_id' => $profile_id])
                ->execute();
        }

        return $response->withStatus(204);
    });

    $this->get('/{id}/banking', function (Request $request, Response $response) {
        $profile_id = $request->getAttribute('id');
        $parsed_params = parse_query_params($request->getQueryParams());
        $fields = $parsed_params['fields'] ?? null;
        $paypal_data = (new QB($this->db))
            ->select('bp_paypal_data', $fields)
            ->where(['profile_id' => $profile_id])
            ->get();
        $bank_data = (new QB($this->db))
            ->select('bp_bank_data', $fields)
            ->where(['profile_id' => $profile_id])
            ->get();
        if ($paypal_data != null) {
            return $response->withJson($paypal_data);
        }
        elseif ($bank_data != null) {
            return $response->withJson($bank_data);
        }
    });

    $this->put('/{id}/banking', function (Request $request, Response $response) {
        $profile_id = $request->getAttribute('id');
        $paypal_data = $request->getParsedBodyParam('paypal_data');
        $bank_data = $request->getParsedBodyParam('bank_data');
        if ($paypal_data) { (new QB($this->db))
                ->update('bp_paypal_data', $paypal_data)
                ->where(['profile_id' => $profile_id])
                ->execute();

            return $response->withStatus(204);
        }
        elseif ($bank_data) { (new QB($this->db))
                ->update('bp_bank_data', $bank_data)
                ->where(['profile_id' => $profile_id])
                ->execute();

            return $response->withStatus(204);
        }
        // TODO: Maybe return some useful status code


    });
});

$app->group('/slot', function () {
    $this->post('', function (Request $request, Response $response) {
        $body = $request->getParsedBody();
        unset($body['transaction']);

        $transaction = $request->getParsedBodyParam('transaction');

        $query = (new QB($this->db))
            ->insert('bp_slot', $body);
        $query->execute();
        $slot_id = $this->db->lastInsertId();

        if ($transaction) {
            $transaction['slot_id'] = $slot_id; (new QB($this->db))
                ->insert('bp_transaction', $transaction)
                ->execute();
        }
        return $response
            ->withStatus(201)
            ->withHeader('Location', API_URL . '/slot/' . $slot_id)
            ->withJson(['id' => $slot_id]);
    });

    $this->get('', function (Request $request, Response $response) {
        $parsed_params = parse_query_params($request->getQueryParams());
        $search = $parsed_params['search'] ?? null;
        $fields = $parsed_params['fields'] ?? null;
        $embed = $parsed_params['embed'] ?? [];
        $query = (new QB($this->db))
            ->select('bp_slot', $fields);
        if ($search) {
            $query = $query->where($search);
        }
        $slots = $query->getAll();
        if (in_array('transaction', $embed)) {
            foreach ($slots as &$slot) {
                $transaction = (new QB($this->db))
                    ->select('bp_transaction')
                    ->where(['slot_id' => $slot['id']])
                    ->get();
                if ($transaction) {
                    // Known PDO issue: all numeric values are converted to string. Parse it back (it's needed in the frontend).
                    $transaction['amount'] = floatval($transaction['amount']);
                    $transaction['cachet'] = $transaction['amount'] * (1 - FEE_PERCENT / 100);
                    $transaction['fee'] = $transaction['amount'] * FEE_PERCENT / 100;
                    $slot['transaction'] = $transaction;
                }
            }
        }
        return $response->withJson($slots);
    });

    $this->get('/{id}', function (Request $request, Response $response) {
        $slot_id = $request->getAttribute('id');
        $parsed_params = parse_query_params($request->getQueryParams());
        $fields = $parsed_params['fields'] ?? null;
        $embed = $parsed_params['embed'] ?? [];
        $slot = (new QB($this->db))
            ->select('bp_slot', $fields)
            ->where(['id' => $slot_id])
            ->get();
        if (in_array('transaction', $embed)) {
            $transaction = (new QB($this->db))
                ->select('bp_transaction')
                ->where(['slot_id' => $slot_id])
                ->get();
            if ($transaction) {
                // Known PDO issue: all numeric values are converted to string. Parse it back (it's needed in the frontend).
                $transaction['amount'] = floatval($transaction['amount']);
                $transaction['cachet'] = $transaction['amount'] * (1 - FEE_PERCENT / 100);
                $transaction['fee'] = $transaction['amount'] * FEE_PERCENT / 100;
                $slot['transaction'] = $transaction;
            }
        }
        return $response->withJson($slot);
    });

    $this->put('/{id}', function (Request $request, Response $response) {
        $slot_id = $request->getAttribute('id');

        $body = [
            'vendor_id' => $request->getParsedBodyParam('vendor_id'),
            'buyer_id' => $request->getParsedBodyParam('buyer_id'),
            'start_time' => $request->getParsedBodyParam('start_time'),
            'category' => $request->getParsedBodyParam('category'),
            'duration' => $request->getParsedBodyParam('duration'),
            'street' => $request->getParsedBodyParam('street'),
            'zip' => $request->getParsedBodyParam('zip'),
            'city' => $request->getParsedBodyParam('city'),
            'country' => $request->getParsedBodyParam('country'),
            'guests_number' => $request->getParsedBodyParam('guests_number'),
            'audio_system' => $request->getParsedBodyParam('audio_system'),
            'lighting_system' => $request->getParsedBodyParam('lighting_system'),
            'catering_included' => $request->getParsedBodyParam('catering_included'),
            'accomodation_included' => $request->getParsedBodyParam('accomodation_included'),
            'cancellation_policy' => $request->getParsedBodyParam('cancellation_policy')
        ]; (new QB($this->db))
            ->update('bp_slot', $body)
            ->where(['id' => $slot_id])
            ->execute();

        return $response->withStatus(204);
    });

    $this->patch('/{id}', function (Request $request, Response $response) {
        $slot_id = $request->getAttribute('id');

        $body = $request->getParsedBody(); (new QB($this->db))
            ->update('bp_slot', $body)
            ->where(['id' => $slot_id])
            ->execute();

        return $response->withStatus(204);
    });

    $this->post('/{id}/transaction', function (Request $request, Response $response) {
        $slot_id = $request->getAttribute('id');
        $body = $request->getParsedBody();
        $body['slot_id'] = $slot_id;
        $query = (new QB($this->db))
            ->insert('bp_transaction', $body);
        $query->execute();

        return $response
            ->withStatus(201)
            ->withHeader('Location', API_URL . '/slot/' . $slot_id . '/transaction');
    });

    $this->get('/{id}/transaction', function (Request $request, Response $response) {
        $slot_id = $request->getAttribute('id');
        $parsed_params = parse_query_params($request->getQueryParams());
        $fields = $parsed_params['fields'] ?? null;

        $transaction = (new QB($this->db))
            ->select('bp_transaction', $fields)
            ->where(['slot_id' => $slot_id])
            ->get();
        // Known PDO issue: all numeric values are converted to string. Parse it back (it's needed in the frontend).
        $transaction['amount'] = floatval($transaction['amount']);
        $transaction['cachet'] = $transaction['amount'] * (1 - FEE_PERCENT / 100);
        $transaction['fee'] = $transaction['amount'] * FEE_PERCENT / 100;
        $transaction['payment_due'] = (new DateTime($transaction['created']))
            ->add(new DateInterval('P2W'))
            ->format(DATE_ISO8601);

        return $response->withJson($transaction);
    });

    $this->patch('/{id}/transaction', function (Request $request, Response $response) {
        $slot_id = $request->getAttribute('id');

        $body = $request->getParsedBody(); (new QB($this->db))
            ->update('bp_transaction', $body)
            ->where(['slot_id' => $slot_id])
            ->execute();

        return $response->withStatus(204);
    });
});

$app->group('/transaction', function () {
    $this->get('/percentages', function (Request $request, Response $response) {
        return $response
            ->withJson([
            'fee' => FEE_PERCENT,
            'cachet' => 100 - FEE_PERCENT
        ]);
    });
});

$app->group('/testo', function() {
    //Testing
    $this->get('', function (Request $request, Response $response) {
        echo "TESTING IT...";
    });
    
});

$app->group('/checkout', function () {
    // Create new checkout credentials.
    $this->post('', function (Request $request, Response $response) {
        /* 
        
        $payment_method = $request->getParsedBodyParam('payment_method');
        
        if ($payment_method !== 'invoice') {
            (new QB($this->db))
            ->update('bp_transaction', [
                    'status' => 'payed',
                    'payment_method' => $payment_method
            ])
            ->where(['slot_id' => $slot_id])
            ->execute();
        } else {
            // Do not update status if payed with invoice.
            (new QB($this->db))
            ->update('bp_transaction', [
                    'payment_method' => $payment_method
            ])
            ->where(['slot_id' => $slot_id])
            ->execute();
        }
        
        */
        
        $slot_id = $request->getParsedBodyParam('slot_id');
        
        // Issue new token.
        $token = issue_checkout_token($slot_id);

        // Send checkout email to buyer.
        send_checkout_mail($token);
        
        return $response
            ->withStatus(201)
            ->withJson(['token' => $token]);
    });

    // Update the transaction status on checkout callbak
    $this->patch('/{token}/{status}', function (Request $request, Response $response) {
        
        //error_log("In patch receive branch for token status", 3, "/var/www/apibandpay/v1/sha.log");
        $body = $request->getParsedBody();
        $token = $request->getAttribute('token');
        $token_data = get_token_data($token);
        $slot_id = $token_data['slot_id'];
        $exp_time = strtotime($token_data['expires']);
    
        //error_log("request get parsed body: " . print_r($request->getParsedBody(), true), 3, "/var/www/apibandpay/v1/sha.log");
    
        // If the token doesn't exist, in this case return
        if (!$token_data) {
            return $response->withStatus(401); // 401 - unauthorized
        }
        // If the token is expired.
        if (time() > $exp_time) {
            return $response->withStatus(410); // 410 - gone
        }

        // Check if event is already paid, and if so, return
        $status = (new QB($this->db))
            ->select('bp_transaction', ['status'])
            ->where(['slot_id' => $slot_id])
            ->getColumn();

        if ($status !== 'to_be_payed') {
            return $response->withStatus(304); // 304 - unmodified
        } 

        $payment_method = $pm;

        if ($payment_method !== 'invoice') {

            // To update the transaction to "paid", first check SHA-OUT signature, ie. compare calculated to given SHA-OUT
            $amount = ($body["amount"]) ? $body["amount"] : $body["AMOUNT"];
            $orderid = ($body["orderID"]) ? $body["orderID"] : $body["ORDERID"];
            $currency = ($body["currency"]) ? $body["currency"] : $body["CURRENCY"];
            $pm = $body["PM"];
            $alias = $body["ALIAS"];
            $acceptance = $body["ACCEPTANCE"];
            $status = $body["STATUS"];
            $cardno = $body["CARDNO"]; 
            $ed = $body["ED"];
            $cn = $body["CN"];
            $trxdate = $body["TRXDATE"];
            $payid = $body["PAYID"];
            $ncerror = $body["NCERROR"];
            $brand = $body["BRAND"];
            $ipcty = $body["IPCTY"];
            $cccty = $body["CCCTY"];
            $eci = $body["ECI"];
            $cvccheck = $body["CVCCheck"];
            $aavcheck = $body["AAVCheck"];
            $vc = $body["VC"];
            $ip = $body["IP"];
            $shasign = $body["SHASIGN"];

            $secretsha = "rWjBj32B?!7D)hUab@mad";

            $tobeshaed = '';
            
            $tobeshaed .= ($aavcheck) ? "AAVCHECK=" . $aavcheck . $secretsha : '';
            $tobeshaed .= ($acceptance) ? "ACCEPTANCE=" . $acceptance . $secretsha : '';
            $tobeshaed .= ($alias) ? "ALIAS=".$alias.$secretsha : '';
            $tobeshaed .= "AMOUNT=" . $amount . $secretsha;
            $tobeshaed .= "BRAND=" . $brand . $secretsha;
            $tobeshaed .= ($cardno) ? "CARDNO=" . $cardno . $secretsha : '';
            $tobeshaed .= ($cccty) ? "CCCTY=" . $cccty . $secretsha : '';
            $tobeshaed .= ($cn) ? "CN=" . $cn . $secretsha : '';
            $tobeshaed .= "CURRENCY=".$currency.$secretsha;
            $tobeshaed .= ($cvccheck) ? "CVCCHECK=" . $cvccheck . $secretsha : '';
            $tobeshaed .= ($eci) ? "ECI=" . $eci . $secretsha : '';
            $tobeshaed .= ($ed) ? "ED=" . $ed . $secretsha : '';
            $tobeshaed .= ($ip) ? "IP=" . $ip . $secretsha : '';
            $tobeshaed .= ($ipcty) ? "IPCTY=" . $ipcty . $secretsha : '';
            $tobeshaed .= ($ncerror | ($ncerror == 0)) ? "NCERROR=" . $ncerror . $secretsha : '';
            $tobeshaed .= ($orderid) ? "ORDERID=" . $orderid . $secretsha : '';
            $tobeshaed .= ($payid) ? "PAYID=" . $payid . $secretsha : '';
            $tobeshaed .= "PM=" . $pm . $secretsha;
            $tobeshaed .= "STATUS=" . $status . $secretsha;
            $tobeshaed .= ($trxdate) ? "TRXDATE=" . $trxdate . $secretsha : '';
            $tobeshaed .= ($vc) ? "VC=" . $vc . $secretsha : '';
            
            $calculatedShaOut = strtoupper(hash("sha1", $tobeshaed, FALSE));
            
            $sha_ok = $shasign == $calculatedShaOut;
            
            if (!$sha_ok) return $response -> withStatus(201);
            
            // If we got here, it means that the calculated SHA-OUT is equal to the one given in the parameters
            error_log("Passed the SHA-OUT check.... ", 3, "/var/www/apibandpay/v1/sha.log");
          
            // Set it as paid
            
            (new QB($this->db))
                ->update('bp_transaction', [
                        'status' => 'payed',
                        'payment_method' => $payment_method
                ])
                ->where(['slot_id' => $slot_id])
                ->execute();
            

            // Send confirmation email to vendor that the money has been paid
            send_confirmation_email_to_vendor($slot_id);
            
            // Send confirmation email to buyer about the successful purchase
            send_confirmation_email_to_buyer($slot_id);
            
            // Send confirmation email to stagend that the transaction has been paid
            send_confirmation_email_to_stagend($slot_id);
            
            
        } else {
            // Do not update status if paid with invoice.
            (new QB($this->db))
            ->update('bp_transaction', [
                    'payment_method' => $payment_method
            ])
            ->where(['slot_id' => $slot_id])
            ->execute();
        }

        return $response
            ->withStatus(201);
            
    });

    // Return data about the token and its slot_id
    $this->get('/{token}', function (Request $request, Response $response) {
        $token = $request->getAttribute('token');
        $token_data = get_token_data($token);

        $exp_time = strtotime($token_data['expires']);

        // If the token doesn't exists.
        if (!$token_data) {
            return $response->withStatus(401);
        }
        // If the token is expired.
        if (time() > $exp_time) {
            return $response->withStatus(410);
        }

        return $response->withJson(['slot_id' => $token_data['slot_id']]);
    });

});

$app->group('/mailing', function () {
});

$app->group('/pdf', function () {
    $this->post('', function (Request $request, Response $response) {
        $html = $request->getParsedBodyParam('html');

        $snappy = new Pdf('/usr/local/bin/wkhtmltopdf.sh');

        $pdf_unique_filename = "invoice_" . uniqid('', TRUE) . ".pdf";
        
        $snappy -> generateFromHtml($html, dirname(dirname(dirname(__FILE__))) . "/bandpay/web/pdfs/" . $pdf_unique_filename);
        //DEV:  $snappy -> generate("https://gigpay.ch/assets/pdf-css/pdf.html", dirname(dirname(dirname(__FILE__))) . "/bandpay/web/pdfs/" . $pdf_unique_filename);

        return $response
            ->withStatus(201)
            ->withHeader('Content-Type', MediaType::JSON)
            ->write(json_encode(json_encode([$pdf_unique_filename])));

    });
});

// Convenient endpoints for the backoffice.
$app->group('/backoffice', function () {
    $this->group('/slot', function () {
        $this->get('', function (Request $request, Response $response) {
            $query = (new QB($this->db))
                ->select('bp_slot s', [
                's.id',
                'v.name vendor',
                'CONCAT_WS(" ", b.first_name, b.last_name) buyer',
                'start_time',
                'CONCAT_WS(" ", t.amount, t.currency) amount',
                'CONCAT_WS(" ", t.amount * (1 - ' . FEE_PERCENT . ' / 100), t.currency) cachet',
                't.payment_method payment_method',
                't.status status',
                't.created'
            ])
                ->join(
                ['bp_profile v', 'bp_buyer b', 'bp_transaction t'],
                [
                    'v.id' => 's.vendor_id',
                    'b.id' => 's.buyer_id',
                    't.slot_id' => 's.id',
                ]
            );
            $slots = $query->getAll();
            return $response->withJson($slots);
        });

        $this->get('/payout', function (Request $request, Response $response) {
            $query = (new QB($this->db))
                ->select('bp_slot s', [
                's.id',
                'v.name vendor',
                'CONCAT_WS(" ", b.first_name, b.last_name) buyer',
                'start_time',
                'CONCAT_WS(" ", t.amount, t.currency) amount',
                'CONCAT_WS(" ", t.amount * (1 - ' . FEE_PERCENT . ' / 100), t.currency) cachet',
                't.payment_method payment_method',
                't.status status',
                't.created'
            ])
                ->join(
                ['bp_profile v', 'bp_buyer b', 'bp_transaction t'],
                [
                    'v.id' => 's.vendor_id',
                    'b.id' => 's.buyer_id',
                    't.slot_id' => 's.id',
                ]
            )
                ->where([
                't.status' => 'payed',
                'UNIX_TIMESTAMP() >= UNIX_TIMESTAMP(s.start_time)'
            ]);
            $slots = $query->getAll();
            // $slots = array_filter($slots, function ($slot) { return time() >= strtotime($slot['start_time']); });

            return $response->withJson($slots);
        });

        $this->get('/confirm-invoice', function (Request $request, Response $response) {
            $query = (new QB($this->db))
                ->select('bp_slot s', [
                's.id',
                'v.name vendor',
                'CONCAT_WS(" ", b.first_name, b.last_name) buyer',
                'start_time',
                'CONCAT_WS(" ", t.amount, t.currency) amount',
                'CONCAT_WS(" ", t.amount * (1 - ' . FEE_PERCENT . ' / 100), t.currency) cachet',
                't.payment_method payment_method',
                't.status status',
                't.created'
            ])
                ->join(
                ['bp_profile v', 'bp_buyer b', 'bp_transaction t'],
                [
                    'v.id' => 's.vendor_id',
                    'b.id' => 's.buyer_id',
                    't.slot_id' => 's.id',
                ]
            )
                ->where([
                't.status' => 'to_be_payed',
                't.payment_method' => 'invoice'
            ]);
            $slots = $query->getAll();
            return $response->withJson($slots);
        });

        $this->get('/closed', function (Request $request, Response $response) {
            $query = (new QB($this->db))
                ->select('bp_slot s', [
                's.id',
                'v.name vendor',
                'CONCAT_WS(" ", b.first_name, b.last_name) buyer',
                'start_time',
                'CONCAT_WS(" ", t.amount, t.currency) amount',
                'CONCAT_WS(" ", t.amount * (1 - ' . FEE_PERCENT . ' / 100), t.currency) cachet',
                't.payment_method payment_method',
                't.status status',
                't.created'
            ])
                ->join(
                ['bp_profile v', 'bp_buyer b', 'bp_transaction t'],
                [
                    'v.id' => 's.vendor_id',
                    'b.id' => 's.buyer_id',
                    't.slot_id' => 's.id',
                ]
            )
                ->where([
                't.status' => 'closed'
            ]);
            $slots = $query->getAll();
            return $response->withJson($slots);
        });

        $this->patch('/{id}/invoice-payed', function (Request $request, Response $response) {
            $slot_id = $request->getAttribute('id');
            
            (new QB($this->db))
            ->update('bp_transaction', [
                'status' => 'payed'
                ])
                ->where([
                    'slot_id' => $slot_id,
                    'payment_method' => 'invoice'
                    ])
                    ->execute();

            return $response->withStatus(204);
        });

        $this->patch('/{id}/closed', function (Request $request, Response $response) {
            $slot_id = $request->getAttribute('id');
            
            (new QB($this->db))
            ->update('bp_transaction', [
                'status' => 'closed'
                ])
                ->where([
                    'slot_id' => $slot_id,
                    'status' => 'payed'
                    ])
                    ->execute();

            return $response->withStatus(204);
        });
    });
});

$app->run();
