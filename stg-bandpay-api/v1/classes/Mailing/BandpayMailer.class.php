<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Bandpay\Mailing;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require dirname(dirname(dirname(dirname(__FILE__)))) . '/vendor/phpmailer/phpmailer/src/Exception.php';
require dirname(dirname(dirname(dirname(__FILE__)))) . '/vendor/phpmailer/phpmailer/src/PHPMailer.php';
require dirname(dirname(dirname(dirname(__FILE__)))) . '/vendor/phpmailer/phpmailer/src/SMTP.php';

/**
 * Description of BandpayMailer
 *
 * @author nathan, Vladimir Despotovic
 */
class BandpayMailer extends PHPMailer {

	public function __construct($exceptions = null) {
		parent::__construct($exceptions);
		
		$this->isSMTP();
		$this->isHTML();
		$this->Debugoutput = 'html';//Ask for HTML-friendly debug output
		$this->SMTPDebug = 2;
		$this->Host = 'smtp.gmail.com'; //Set the hostname of the mail server
		$this->Port = 587; //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$this->SMTPSecure = 'tls'; //Set the encryption system to use - ssl (deprecated) or tls
		$this->SMTPAuth = true; // Whether to use SMTP authentication
		$this->Username = "romanija82@gmail.com"; //Username to use for SMTP authentication - use full email address for gmail
		$this->Password = "sifrarade"; //Password to use for SMTP authentication
		$this->setFrom('noreply@bandpay.com', 'Bandpay'); //Set who the message is to be sent from
		$this->addReplyTo('noreply@bandpay.com', 'Bandpay'); //Set an alternative reply-to address

		//Read an HTML message body from an external file, convert referenced images to embedded,
		//convert HTML into a basic plain-text alternative body
		//$this->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));

		$this->AltBody = 'This is a plain-text message body'; //Replace the plain text body with one created manually

	}
	
	public function bodyFromHtml(string $filename, array $templateParams = []): void {
		$skeleton = file_get_contents(API_ROOT . '/mail_templates/skeleton.html');
		$template = str_replace('{{body}}', file_get_contents($filename), $skeleton);
		$mail = preg_replace_callback('/{{(.+?)}}/', function (array $matches) use ($templateParams) {
			return $templateParams[$matches[1]];
		}, $template);
		
		$this -> msgHTML($mail, __DIR__);
	}

}
