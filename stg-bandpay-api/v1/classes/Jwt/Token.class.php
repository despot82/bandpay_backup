<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Bandpay\JWT;

use DateTime;

/**
 * Description of Token
 *
 * @author nathan
 */
final class Token {
	public static $exp_after = '+1 week';
	public static $alg = 'HS256';

	public static function getSecret() {
		$conn = \Bandpay\Database\DB::get();
		$stm = $conn->query('SELECT * FROM bp_jwt_secret;');
		return $stm->fetchColumn();
	}
	
	// TODO: Handle null secret case.
	public static function issue(string $secret, array $payloadExtra = []) {
		if ($secret !== static::getSecret()) {
			return FALSE;
		}
		
		$issued_at = new DateTime();
		$not_before = $issued_at;
		$expire_date = new DateTime(static::$exp_after);

		$issuer = $_SERVER['SERVER_NAME'];
		$jti = uniqid();

		$payload = array_merge([
			'iat' => $issued_at->getTimestamp(),
			'nbf' => $not_before->getTimestamp(),
			'exp' => $expire_date->getTimestamp(),
			'iss' => $issuer,
			'jti' => $jti
		], $payloadExtra);
		return \Firebase\JWT\JWT::encode($payload, $secret, static::$alg);
	}
}
