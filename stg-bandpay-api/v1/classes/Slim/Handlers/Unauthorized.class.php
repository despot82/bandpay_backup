<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Slim\Handlers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Body;

/**
 * Description of Unauthorized
 *
 * @author nathan
 */
class Unauthorized extends AbstractHandler {

	/**
	 * Invoke unauthorized handler
	 *
	 * @param  ServerRequestInterface $request  The most recent Request object
	 * @param  ResponseInterface      $response The most recent Response object
	 *
	 * @return ResponseInterface
	 * @throws UnexpectedValueException
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, string $authMethod) {
		$contentType = $this->determineContentType($request);
		switch ($contentType) {
			case 'application/json':
				$output = $this->renderJsonUnauthorizedOutput();
				break;

			case 'text/xml':
			case 'application/xml':
				$output = $this->renderXmlUnauthorizedOutput();
				break;

			case 'text/html':
				$output = $this->renderHtmlUnauthorizedOutput($authMethod);
				break;

			default:
				throw new UnexpectedValueException('Cannot render unknown content type ' . $contentType);
		}

		$body = new Body(fopen('php://temp', 'r+'));
		$body->write($output);

		return $response->withStatus(401)
						->withHeader('Content-Type', $contentType)
						->withHeader('WWW-Authenticate', $authMethod)
						->withBody($body);
	}

	/**
	 * Return a response for application/json content unauthorized
	 *
	 * @return string
	 */
	protected function renderJsonUnauthorizedOutput() {
		return '{"message":"Access Denied"}';
	}

	/**
	 * Return a response for xml content unauthorized
	 *
	 * @return string
	 */
	protected function renderXmlUnauthorizedOutput() {
		return '<root><message>Access Denied</message></root>';
	}

	/**
	 * Return a response for text/html content unauthorized
	 *
	 * @param  string $authMethod  The most recent Request object
	 *
	 * @return string
	 */
	protected function renderHtmlUnauthorizedOutput(string $authMethod) {
		return <<<END
<html>
    <head>
        <title>Access Denied</title>
        <style>
            body{
                margin:0;
                padding:30px;
                font:12px/1.5 Helvetica,Arial,Verdana,sans-serif;
            }
            h1{
                margin:0;
                font-size:48px;
                font-weight:normal;
                line-height:48px;
            }
            strong{
                display:inline-block;
                width:65px;
            }
        </style>
    </head>
    <body>
        <h1>Access Denied</h1>
        <p>
            Access to the requested resource is denied. Wrong credentials supplied or no credentials at all. Expected authentication method is <b>$authMethod</b>.
        </p>
    </body>
</html>
END;
	}

}
