<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MediaType
 *
 * @author nathan
 */
class MediaType {
	const JAVASCRIPT =	'application/javascript';
	const JSON =		'application/json';
	const XML =			'application/xml';
	const ZIP =			'application/zip';
	const PDF =			'application/pdf';
	const CSS =			'text/css';
	const HTML =		'text/html';
	const PLAIN =		'text/plain';
	const PNG =			'image/png';
	const JPEG =		'image/jpeg';
	const GIF =			'image/gif';
}
