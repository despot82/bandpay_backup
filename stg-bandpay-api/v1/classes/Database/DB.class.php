<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Bandpay\Database;

use PDO;

/**
 * Description of DB
 *
 * @author nathan
 */
class DB {

	const HOST = 'localhost';
	const DATABASE = 'bandpay';
	const USER = 'root';
	const PASSWORD = 'root';
	const OPTIONS = [
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
	];
	
	/**
	 *
	 * @var DB 
	 */
	protected static $instance;
	
	protected $pdo;

	protected function __construct() {
		$this->pdo = new PDO('mysql:host=' . static::HOST . ';dbname=' . static::DATABASE, static::USER, static::PASSWORD, static::OPTIONS);
	}

	private function __clone() {
		
	}

	private function __wakeup() {
		
	}

	/**
	 * 
	 * @return PDO
	 */
	public static function get() {
		if (!isset(static::$instance)) {
			static::$instance = new static();
		}
		return static::$instance->pdo;
	}
}
