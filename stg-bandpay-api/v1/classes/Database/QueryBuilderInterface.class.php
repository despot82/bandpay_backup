<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Bandpay\Database;

/**
 *
 * @author nathan
 */
interface QueryBuilderInterface {
	public function select(string $table, array $columns = NULL): QueryBuilderInterface;
	public function insert(string $table, array $values): QueryBuilderInterface;
	public function update(string $table, array $values): QueryBuilderInterface;
	public function delete(string $table): QueryBuilderInterface;
	
	public function where(array $conditions): QueryBuilderInterface;
	public function join(array $tables, array $conditions): QueryBuilderInterface;
	public function onDuplicateKeyUpdate(array $values): QueryBuilderInterface;
	public function orderBy(array $columns, bool $asc): QueryBuilderInterface;
	
	public function execute(): void;
	public function get();
	public function getAll();
	public function getColumn(int $column_number = 0);
}
