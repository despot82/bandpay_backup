<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Bandpay\Database;

include API_ROOT . '/lib/array_helper.lib.php';

use PDO;

/**
 * Description of QueryBuilder
 *
 * @author nathan
 */
class MySQLQueryBuilder implements QueryBuilderInterface
{

    /**
     *
     * @var PDO Database connection instance.
     */
    protected $pdo;

    /**
     *
     * @var string The query.
     */
    protected $query;

    /**
     *
     * @var array
     */
    protected $parameters;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->query = '';
        $this->parameters = [];
    }
    
    /**
     *
     * @param string $table
     */
    public function delete(string $table): QueryBuilderInterface
    {
        $this->query .= "DELETE FROM $table ";
        return $this;
    }
    
    /**
     *
     * @param string $table
     * @param array $values
     * @return \Bandpay\Database\QueryBuilderInterface
     */
    public function insert(string $table, array $values): QueryBuilderInterface
    {
        // First case: column => value pair (single row).
        if (array_is_assoc($values)) {
            $columns_names = implode(',', array_keys($values));
            $values_str = implode(',', array_fill(0, count($values), '?'));
            $params = array_map(function ($value) {
                // Map boolean values to 1 and 0 since MySQL doesn't have boolean type.
                return is_bool($value) ? $value ? 1 : 0 : $value;
            }, array_values($values));
            $this->parameters = array_merge($this->parameters, $params);
            $this->query .= "INSERT INTO $table ($columns_names) VALUES ($values_str) ";
        } else {
            // Second case: column => value pair (multiple rows: array of arrays).
            if (is_array($values[0])) {
                $columns_names = implode(',', array_keys($values[0]));
                $values_arr = array_map(function ($value) {
                    return '(' . implode(',', array_fill(0, count($value), '?')) . ')';
                }, $values);
                $values_str = implode(',', $values_arr);
                $params = [];
                array_walk_recursive($values, function ($value) use (&$params) {
                    array_push($params, $value);
                });
                $this->parameters = array_merge($this->parameters, $params);
                $this->query .= "INSERT INTO $table ($columns_names) VALUES ($values_str) ";
            } else {
                // Third case: only column name, values retrieved from select statement.
                $columns_names = implode(',', $values);
                $this->query .= "INSERT INTO $table ($columns_names) ";
            }
        }
        return $this;
    }

    public function onDuplicateKeyUpdate(array $values): QueryBuilderInterface
    {
        $values_array = [];
        array_walk($values, function ($value, $key) use (&$values_array) {
            // Performs a regular expression match to find out if the VALUES keyword is used and then acts accordingly.
            if (preg_match('/VALUES\(.+\)/', $value)) {
                array_push($values_array, "$key = $value");
            } else {
                array_push($values_array, "$key = ?");
                array_push($this->parameters, $value);
            }
        });
        $values_str = implode(',', $values_array);
        $this->query .= "ON DUPLICATE KEY UPDATE $values_str ";
        return $this;
    }

    public function orderBy(array $columns, bool $asc = true): QueryBuilderInterface
    {
        $columns_names = implode(',', $columns);
        $asc_str = $asc ? 'ASC' : 'DESC';
        $this->query .= "ORDER BY $columns_names $asc_str ";
        return $this;
    }

    public function select(string $table, array $columns = null): QueryBuilderInterface
    {
        $columns_names = $columns ? implode(',', $columns) : '*';
        $this->query .= "SELECT $columns_names FROM $table ";
        return $this;
    }

    public function update(string $table, array $values): QueryBuilderInterface
    {
        $values_arr = array_map(function ($val) {
            return "$val=?";
        }, array_keys($values));
        $values_str = implode(',', $values_arr);
        $this->query .= "UPDATE $table SET $values_str ";
        $this->parameters = array_merge($this->parameters, array_values($values));
        return $this;
    }

    public function where(array $conditions): QueryBuilderInterface
    {
        $condition_columns = [];
        array_walk($conditions, function ($value, $key) use (&$condition_columns) {
            if (is_int($key)) {
                // Simple condition (without parameters).
                array_push($condition_columns, $value);
            } else {
                // Condition with parameters.
                // If the operator isn't provided assume '='.
                if (!preg_match('/<|>|<=|>=/', $key)) {
                    array_push($condition_columns, "$key = ?");
                    array_push($this->parameters, $value);
                } else {
					array_push($condition_columns, "$key ?");
                    array_push($this->parameters, $value);
				}
            }
        });
        $conditions_str = implode(' AND ', $condition_columns);
        $this->query .= "WHERE $conditions_str ";
        return $this;
    }

    public function join(array $tables, array $conditions): QueryBuilderInterface
    {
        $tables_names = implode(',', $tables);
        $conditions_array = [];
        array_walk($conditions, function ($value, $key) use (&$conditions_array) {
            array_push($conditions_array, "$key = $value");
        });
        $conditions_str = implode(' AND ', $conditions_array);
        $this->query .= "JOIN ($tables_names) ON ($conditions_str) ";
        return $this;
    }

    public function execute(): void
    {
        $stm = $this->pdo->prepare($this->query);
        $stm->execute($this->parameters);
    }

    public function get(int $fetch_style = null)
    {
        $stm = $this->pdo->prepare($this->query);
        $stm->execute($this->parameters);
        return $stm->fetch($fetch_style);
    }

    public function getAll(int $fetch_style = null)
    {
        $stm = $this->pdo->prepare($this->query);
        $stm->execute($this->parameters);
        return $stm->fetchAll($fetch_style);
    }

    public function getColumn(int $column_number = 0)
    {
        $stm = $this->pdo->prepare($this->query);
        $stm->execute($this->parameters);
        return $stm->fetchColumn($column_number);
    }
}
