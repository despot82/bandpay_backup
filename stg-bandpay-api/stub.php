<?php
/**
 * Generated stub file for code completion purposes
 */



namespace HRTime;

class PerformanceCounter
{
    public function getFrequency() {}
    public function getTicks() {}
    public function getTicksSince() {}
}

namespace HRTime;

class StopWatch extends HRTime\PerformanceCounter
{
    public function start() {}
    public function stop() {}
    public function getElapsedTicks() {}
    public function getLastElapsedTicks() {}
    public function isRunning() {}
    public function getElapsedTime() {}
    public function getLastElapsedTime() {}
    public function getFrequency() {}
    public function getTicks() {}
    public function getTicksSince() {}
}

namespace HRTime;

class Unit
{
    const SECOND = 0;
    const MILLISECOND = 1;
    const MICROSECOND = 2;
    const NANOSECOND = 3;
}

